import Data.List
import Data.Map (Map)
import qualified Data.Map as M

-- data Direction = E | SE | SW | W | NW | NE deriving Show
-- Just use coordinates directly
type Direction = (Int, Int)

parseDir :: String -> (String, Direction)
parseDir s
  | isPrefixOf "e" s = (drop 1 s, (1,0))
  | isPrefixOf "se" s = (drop 2 s, (1, -1))
  | isPrefixOf "sw" s = (drop 2 s, (0, -1))
  | isPrefixOf "w" s = (drop 1 s, (-1,0))
  | isPrefixOf "nw" s = (drop 2 s, (-1, 1))
  | isPrefixOf "ne" s = (drop 2 s, (0,1))


parseLine s = while ((>0) . length) parseDir s []
  where
    while p f x y
      | p x = while p f (fst $ f x) (y ++ [snd $ f x])
      | otherwise = y

accSteps ls = foldl (\acc s -> (fst acc + fst s, snd acc + snd s)) (0, 0) ls






main = do
  contents <- lines <$> readFile "input.txt"
  let positions = map (accSteps . parseLine) contents
      counts = foldl (\m p -> M.insertWith (+) p 1 m) M.empty positions
      back2Black = filter odd $ M.elems counts     
  
  print $ length back2Black
  print "It's Christmas Eve!"
