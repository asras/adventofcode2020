import Data.List
import Data.Map (Map)
import qualified Data.Map as M

-- data Direction = E | SE | SW | W | NW | NE deriving Show
-- Just use coordinates directly
type Direction = (Int, Int)

parseDir :: String -> (String, Direction)
parseDir s
  | isPrefixOf "e" s = (drop 1 s, (1,0))
  | isPrefixOf "se" s = (drop 2 s, (1, -1))
  | isPrefixOf "sw" s = (drop 2 s, (0, -1))
  | isPrefixOf "w" s = (drop 1 s, (-1,0))
  | isPrefixOf "nw" s = (drop 2 s, (-1, 1))
  | isPrefixOf "ne" s = (drop 2 s, (0,1))




-- Map contains black tiles
-- Fold over keys, adding 1 to every neighbor tile and 100 to self
-- Tiles with count = 2 or count = 101 or count = 102 are live next round
step :: Map (Int, Int) Int -> Map (Int, Int) Int
step m = M.filter checkCount $ foldl (\accm k -> addCounts accm k) M.empty $ M.keys m
  where
    addCounts ma key = foldl (\acm n -> M.insertWith (+) (addTuple key n) (val n) acm) ma neighbors
    val n = if n == (0, 0) then 100 else 1

    addTuple (a, b) (c, d) = (a + c, b + d)
    neighbors = [(0, 0), (1, 0), (0, 1), (-1, 1), (-1, 0), (0, -1), (1, -1)]
    checkCount x = x == 2 || x == 101 || x == 102

parseLine s = while ((>0) . length) parseDir s []
  where
    while p f x y
      | p x = while p f (fst $ f x) (y ++ [snd $ f x])
      | otherwise = y

accSteps ls = foldl (\acc s -> (fst acc + fst s, snd acc + snd s)) (0, 0) ls






main = do
  contents <- lines <$> readFile "input.txt"
  let positions = map (accSteps . parseLine) contents
      counts = foldl (\m p -> M.insertWith (+) p 1 m) M.empty positions
      start = M.filter odd counts
      back2Black = M.keys $ head $ drop 100 $ iterate step start     

  print $ length back2Black
  print "It's Christmas Eve!"
