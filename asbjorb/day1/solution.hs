import System.IO

allPairs :: [a] -> [(a, a)]
allPairs [] = []
allPairs (x:xs)
  | length xs == 0 = []
  | otherwise      = [(x, other) | other <- xs] ++ allPairs xs


sumIs2020 = filter (\(a, b) -> a + b == 2020)

solutions :: [Int] -> [Int]
solutions ls = map (\(a, b) -> a * b) $ sumIs2020 $ allPairs ls


nonEmptyLines :: String -> [String]
nonEmptyLines contents = filter (\l -> length l > 0) $ lines contents

parseFloat :: String -> Int
parseFloat s = read s
  
main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let items = nonEmptyLines contents
          values = map parseFloat items
      mapM_ putStrLn $ map show $ solutions values
      putStrLn "Oh yeah")
