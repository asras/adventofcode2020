import System.IO

allPairs :: [a] -> [(a, a)]
allPairs [] = []
allPairs (x:xs)
  | length xs == 0 = []
  | otherwise      = [(x, other) | other <- xs] ++ allPairs xs


allTriples (x:xs) = [(x, a, b) | (a, b) <- allPairs xs] ++ allTriples xs
allTriples _ = []


tripleSumIs2020 = filter (\(a, b, c) -> a + b + c == 2020)

solutions :: [Int] -> [Int]
solutions ls = map (\(a, b, c) -> a * b * c) $ tripleSumIs2020 $ allTriples ls


nonEmptyLines :: String -> [String]
nonEmptyLines contents = filter (\l -> length l > 0) $ lines contents

parseFloat :: String -> Int
parseFloat s = read s
  
main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let items = nonEmptyLines contents
          values = map parseFloat items
      mapM_ putStrLn $ map show $ solutions values
      putStrLn "Oh yeah again")
