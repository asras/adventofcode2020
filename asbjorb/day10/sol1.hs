import System.IO
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

-- We don't skip anything so we always have to choose the minimum
-- from the list. Accumulate step sizes
getDiffs :: [Int] -> [Int]
getDiffs ls = diffs
  where
    sls = 0:(sort ls) ++ [maximum ls + 3]
    diffs = [sls !! (i+1) - sls !! (i) | i <- [0..(length sls - 2)]]

countOcc :: [Int] -> Map Int Int
countOcc ls = helper startMap ls
  where
    startMap = Map.empty :: Map Int Int
    helper acc [] = acc
    helper acc (x:xs)
      | Map.member x acc = helper (Map.adjust (+1) x acc) xs
      | otherwise = helper (Map.insert x 1 acc) xs
main = do
  contents <- lines <$> readFile "input.txt"
  let nums = map read contents :: [Int]
      occs = countOcc $ getDiffs nums
      num1 = occs Map.! 1
      num3 = occs Map.! 3

  print $ "The answer to your query is: " ++ (show (num1 * num3))
  print "I don't think there are 25 different christmas greetings."
