import System.IO
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

nextChoices :: [Int] -> Int -> [Int]
nextChoices ls i = sort $ filter (\x -> x > i && i+3 >= x) ls

-- We search from n to end
-- The first dict holds connections from key
-- The second dict holds results of previous searches
-- In the end we return the search dict
search :: Int -> Map Int [Int] -> Map Int Int -> Map Int Int
search n choi seen
  | n == (maximum $ map fst $ Map.toList choi) = Map.insert n 1 seen
search n choi seen
  | Map.member n seen = seen
search n choi seen = newseen
  where
    neighbors = choi Map.! n
    -- For each neighbor we search from them to end, updating seen dict
    -- along the way.
    -- The number of paths from n to end is then the sum
    -- of the numbers each neighbor has.
    -- To keep updating the dict we need to use fold.
    searchothers = foldr ((\ne se -> search ne choi se)) seen neighbors
    theirnumbers = map (\ne -> searchothers Map.! ne) neighbors
    newseen = Map.insert n (sum theirnumbers) searchothers
    
choices :: [Int] -> Map Int [Int]
choices ls = themap
  where
    sls = sort ls
    themap = foldr (\el m -> Map.insert el (nextChoices ls el) m) (Map.empty) $ sls


main = do
  contents <- lines <$> readFile "input.txt"
  let nums = sort $ map read contents :: [Int]

  print $ search 0 (choices (0:nums)) (Map.empty) Map.! 0
  print $ "Easy peasy"
  print "I don't think there are 25 different christmas greetings."
