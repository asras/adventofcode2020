import System.IO


neighbors i j board = map selectSquares $ filter insideBoard indices
  where
    indices = [(i', j') | i' <- [i-1, i, i+1], j' <- [j-1, j, j+1], not (i' == i && j' == j)]
    insideBoard (a, b)
      | a < 0 || b < 0 = False
      | a >= nrows || b >= ncols = False
      | otherwise = True
    nrows = length board
    ncols = length (board !! 0)
    selectSquares (a, b) = (board !! a) !! b

no p ls = all (\x -> not (p x)) ls
-- Define rules for updating a single square (char)
-- based on its neighborhood (list of char/string)
rule :: Char -> String -> Char
rule '.' _ = '.'
rule 'L' ns
  | no (=='#') ns = '#'
  | otherwise = 'L'
rule '#' ns
  | (sum $ map (fromEnum . (=='#')) ns) >= 4 = 'L'
  | otherwise = '#'

type Board = [String]
updateBoard :: Board -> Board
updateBoard board = [[rule ((board !! i) !! j) (neighbors i j board)
                     | j <- [0..ncols-1]]
                     | i <- [0..nrows-1]]
  where
    nrows = length board
    ncols = length (board !! 0)

countOcc :: Board -> Int
countOcc board = foldl (+) 0 $ map (sum . (map (fromEnum . (=='#')))) board

converge 0 _ _ = error "did not converge"
converge i f x = let x' = f x in if x' == x then x' else converge (i-1) f (f x)

msg a = "Conway finds the answer to be: " ++ (show a)

main = interact $ (++"Merry Christmas to you!\n") . (++"\n") . show . msg . countOcc . (converge 50000 updateBoard) . lines
