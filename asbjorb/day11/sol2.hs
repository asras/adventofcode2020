import System.IO
import Data.Map (Map)
import qualified Data.Map as Map

{-
Tried to do a performance optimization by using a Map.
That failed.
-}

--type Board = Map (Int, Int) Char
type Board = [String]


walk (i, j) (maxi, maxj) (di, dj) = takeWhile test [(i + di*k, j + dj*k) | k <- [1..]]
  where
    test (a, b) = a >= 0 && b >= 0 && a <= maxi && b <= maxj

neighbors :: (Int, Int) -> Board -> [Char]
neighbors (i, j) board = ns
  where
    directions = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
    ns = foldl (\tls sl -> tls ++ (firstNonEmpty sl)) [] $ map (walk (i, j) (maxi, maxj)) directions
    firstNonEmpty [] = []
    firstNonEmpty (x:xs)
      | val /= '.' = [val]
      | otherwise = firstNonEmpty xs
      where
        val = (board !! fst x) !! snd x
    maxi = length board - 1
    maxj = length (board !! 0) - 1

-- neighbors (i, j) board = ns
--   where
--     directions = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
--     ns = foldl (\tls sl -> tls ++ (firstNonEmpty sl)) [] $ map (walk (i, j) (maxi, maxj)) directions
--     firstNonEmpty [] = []
--     firstNonEmpty (x:xs)
--       | val /= '.' = [val]
--       | otherwise = firstNonEmpty xs
--       where
--         val = board Map.! x
--     maxi = (maximum $ map fst $ Map.keys board)
--     maxj = (maximum $ map snd $ Map.keys board)

no p ls = all (\x -> not (p x)) ls
-- Define rules for updating a single square (char)
-- based on its neighborhood (list of char/string)
rule :: Char -> String -> Char
rule '.' _ = '.'
rule 'L' ns
  | no (=='#') ns = '#'
  | otherwise = 'L'
rule '#' ns
  | (sum $ map (fromEnum . (=='#')) ns) >= 5 = 'L'
  | otherwise = '#'


updateBoard :: Board -> Board
updateBoard board = [[rule ((board !! i) !! j) (ns i j) | j <- [0..ncols-1]] | i <- [0..nrows-1]]
  where
    ns i j = neighbors (i, j) board
    nrows = length board
    ncols = length (board !! 0)

-- updateBoard board = foldl (\tmap indcs -> updater tmap indcs) board indices
--   where
--     applyRule indcs = rule (board Map.! indcs) (neighbors indcs board)
--     updater tmap indcs = Map.insert indcs (applyRule indcs) tmap
--     indices = [(i, j) | i <- [0..imax], j <- [0..jmax]]
--     imax = maximum $ map fst $ Map.keys board
--     jmax = maximum $ map snd $ Map.keys board

toBoard :: [String] -> Board
toBoard = id
-- toBoard sboard = foldl folder Map.empty $ zip [0..] sboard
--   where
--     folder tmap (index, strng) = foldl (folder' index) tmap $ zip [0..] strng
--     folder' i tmap' (index', ch) = Map.insert (i, index') ch tmap'
    
countOcc :: Board -> Int
countOcc board = sum $ map (fromEnum . (=='#')) entries
  where
    entries = [(board !! i) !! j | i <- [0..imax], j <- [0..jmax]]
    imax = length board - 1
    jmax = length (board !! 0) - 1
-- countOcc board = sum $ map (fromEnum . (=='#')) entries
--   where
--     entries = [board Map.! (i, j) | i <- [0..imax], j <- [0..jmax]]
--     imax = maximum $ map fst $ Map.keys board
--     jmax = maximum $ map snd $ Map.keys board

    

converge 0 _ _ = error "did not converge"
converge i f x = let x' = f x in if x' == x then x' else converge (i-1) f (f x)

msg a = "Conway finds the answer to be: " ++ (show a)

main = interact $ (++"Merry Christmas to you!\n") . (++"\n") . show . msg . countOcc . (converge 2000 updateBoard) . toBoard . lines
