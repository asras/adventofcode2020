import System.IO

-- I am going to assume we can only face N, E, S, or W
-- so that rotations are always multiples of 90 degrees
-- Ship has a direction and the position on the gird
data Ship = Ship { dir :: (Int, Int),
                   pos :: (Int, Int)} deriving Show

-- Data type for the instruction that the ship can get
data Instruction = F Int | N Int | E Int | S Int | W Int | R Int | L Int deriving Show


apply :: Ship -> Instruction -> Ship
apply (Ship (dx, dy) (x, y)) (F n) = Ship (dx, dy) (x + dx * n, y + dy * n)
apply (Ship (dx, dy) (x, y)) (N n) = Ship (dx, dy) (x, y + n)
apply (Ship (dx, dy) (x, y)) (E n) = Ship (dx, dy) (x + n, y)
apply (Ship (dx, dy) (x, y)) (S n) = Ship (dx, dy) (x, y - n)
apply (Ship (dx, dy) (x, y)) (W n) = Ship (dx, dy) (x - n, y)
apply ship                   (L n) = apply ship (R (-n))
apply (Ship (dx, dy) (x, y)) (R n) = Ship (dx', dy') (x, y)
  where
    -- Convert direction vector to enum
    dir 0 1    = 0
    dir 1 0    = 1
    dir 0 (-1) = 2
    dir (-1) 0 = 3

    rotations = if mod n 90 /= 0 then error "Bad rotation" else div n 90
    newdir = mod ((dir dx dy) + rotations) 4

    undir 0 = (0, 1)
    undir 1 = (1, 0)
    undir 2 = (0, -1)
    undir 3 = (-1, 0)

    dx' = fst $ undir newdir
    dy' = snd $ undir newdir


ti = [F 10, N 3, F 7, R 90, F 11]
sh = Ship (1, 0) (0, 0)
r = foldl (\s i -> apply s i) sh ti

distance :: Ship -> Int
distance (Ship _ (x, y)) = (abs x) + (abs y)


parse :: String -> Instruction
parse ('F':num) = F (read num)
parse ('N':num) = N (read num)
parse ('E':num) = E (read num)
parse ('S':num) = S (read num)
parse ('W':num) = W (read num)
parse ('R':num) = R (read num)
parse ('L':num) = L (read num)
parse s = error ("Got: " ++ s)

main = do
  contents <- lines <$> readFile "input.txt"
  let instructions = map parse contents
      finalShip = foldl (\ship inst -> apply ship inst) (Ship (1, 0) (0, 0)) instructions
  print $ (++) "Ahoy matey, the final distance be: " $ show $ distance finalShip
  print "Arrgh it do be a Merry Christmas!"
