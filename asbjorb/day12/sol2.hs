import System.IO

-- Improvements (?): Make directions a single vector instead of sum type


-- I am going to assume we can only face N, E, S, or W
-- so that rotations are always multiples of 90 degrees
-- Ship has a direction and the position on the gird
-- For part 2 the direction is obsolete and instead becomes the position
-- of the waypoint.
data Ship = Ship { wpos :: (Int, Int),
                   pos :: (Int, Int)} deriving Show

-- Data type for the instruction that the ship can get
data Instruction = F Int | N Int | E Int | S Int | W Int | R Int | L Int deriving Show


apply :: Ship -> Instruction -> Ship
apply (Ship (dx, dy) (x, y)) (F n) = Ship (dx, dy) (x + dx * n, y + dy * n)
apply (Ship (dx, dy) (x, y)) (N n) = Ship (dx, dy + n) (x, y)
apply (Ship (dx, dy) (x, y)) (E n) = Ship (dx + n, dy) (x, y)
apply (Ship (dx, dy) (x, y)) (S n) = Ship (dx, dy - n) (x, y)
apply (Ship (dx, dy) (x, y)) (W n) = Ship (dx - n, dy) (x, y)
apply ship (L n)                   = apply ship (R (-n))
apply (Ship (dx, dy) (x, y)) (R n) = Ship wpos (x, y)
  where
    -- Should have done this in part 1
    rotateR (a, b) = (b, -a)
    rotateL (a, b) = (-b, a)
    rotations = if mod n 90 /= 0 then error "Bad rotation" else div n 90
    rotater = if rotations < 0 then rotateL else rotateR
    wpos = head $ drop (abs rotations) $ iterate rotater (dx, dy)


ti = [F 10, N 3, F 7, R 90, F 11]
sh = Ship (10, 1) (0, 0)
r = foldl (\s i -> apply s i) sh ti

distance :: Ship -> Int
distance (Ship _ (x, y)) = (abs x) + (abs y)


parse :: String -> Instruction
parse ('F':num) = F (read num)
parse ('N':num) = N (read num)
parse ('E':num) = E (read num)
parse ('S':num) = S (read num)
parse ('W':num) = W (read num)
parse ('R':num) = R (read num)
parse ('L':num) = L (read num)
parse s = error ("Got: " ++ s)

main = do
  contents <- lines <$> readFile "input.txt"
  let instructions = map parse contents
      finalShip = foldl (\ship inst -> apply ship inst) (Ship (10, 1) (0, 0)) instructions
  print $ (++) "Ahoy matey, the final distance be: " $ show $ distance finalShip
  print "Arrgh it do be a Merry Christmas!"
