import System.IO
import Data.List.Split (splitOn)
import Text.Read (readMaybe)
import Data.Maybe (mapMaybe)
import Data.List (minimumBy)

parse :: String -> [Int]
parse st = mapMaybe (readMaybe :: String -> Maybe Int) $ splitOn "," st


orderSnd x y
  | snd x > snd y = GT
  | snd x < snd y = LT
  | otherwise     = EQ

main = do
  contents <- readFile "input.txt"
  let [timestr, busstrs] = lines contents
      time = read timestr :: Int
      busids = parse busstrs
      waittimes = map (\x -> (x, x - (mod time x))) busids
      res = minimumBy orderSnd waittimes
  print $ fst res * snd res
  print "Merry Christmas across the land"
