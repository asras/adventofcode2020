import System.IO
import Data.List.Split (splitOn)
import Text.Read (readMaybe)
import Data.Maybe (mapMaybe)
import Data.List (minimumBy)

-- We change the type of parse to return the id as the fst
-- and the id-number as the snd
parse :: String -> [(Integer, Integer)]
parse st = mapMaybe readStuff $ zip [0..] $ splitOn "," st
  where
    readStuff (a, b) = case readMaybe b :: Maybe Integer of
                         Just x -> Just (x, a)
                         Nothing -> Nothing

-- After some thinking and googling I find that this type of
-- problem is called a system of congruences. It relies on the
-- fact that all the ns are coprime (I checked this).
-- Given two congruences
-- x = a1 (mod n1)
-- x = a2 (mod n2)
-- We can find a solution
-- x = a12 (mod n1*n2)
-- which is unique.
-- Given a set of such equations
-- x = a1 (mod n1)
-- x = a2 (mod n2)
-- x = a3 (mod n3)
-- ...
-- We can then reduce it using the solution of two congruences
-- until we have one equation:
-- x = a12..K (mod n1*n2*...*nK)
-- and a12..K will be the smallest positive integer that solves
-- all the congruences.
-- So let's start by writing a function to solve 2 congruences

-- The algorithm to solve two congruences is to first find
-- m1, m2 such that m1n1 + m2n2 = 1 (= gcd(n1, n2)).
-- This can be done with the extend Euclidean algorithm
-- Then a solution for x is
-- x = a1m2n2 + a2m1n1
-- Proof: x = a1m2n2 + a2m1n1 = a1(1-m1n1) + a2m1n1 = a1 (a2-a1)m1n1.
-- Proof is similar for x = a2 (mod n2).

-- The extended euclidean algorithm is
extEuc (r0, s0, t0) (r1, s1, t1)
  | r1 == 0 = (r0, s0, t0)
extEuc (r0, s0, t0) (r1, s1, t1) = extEuc (r1, s1, t1) (r0 - q*r1, s0 - q*s1, t0 - q*t1)
  where
    getR qi = r0 - qi * r1
    findQ qi = let rnext = getR qi in if rnext >= 0 && rnext < r1
                                      then qi
                                      else findQ (qi - 1)
    q = findQ (div r0 r1)

-- After running this with input (n1, 1, 0) (n2, 0, 1)
-- we have (1, m1, m2) = output
-- First element should be 1 because n1 and n2 are coprime.

data Congruence = C {rhs :: Integer, modn :: Integer} deriving Show

solveCongs :: Congruence -> Congruence -> Congruence
solveCongs (C _ n1) (C _ n2) | (gcd n1 n2) /= 1 = error ("Can only solve coprime congruences" ++ (show n1) ++ " " ++ (show n2))
solveCongs (C a1 n1) (C a2 n2) = C x (n1*n2)
  where
    (1, m1, m2) = extEuc (n1, 1, 0) (n2, 0, 1)
    x = mod ((a1*m2*n2 + a2*m1*n1)) (n1 * n2)

-- Test congruences
c1 = C 2 3
c2 = C 3 5
-- Solution should be C 8 15
s1 = solveCongs c1 c2

c3 = C 5 7
-- Solution to c2 && c3 should be C 33 35
s2 = solveCongs c2 c3


-- Now we can solve a system of congruences
solveMany :: [Congruence] -> Congruence
solveMany cs = foldl (\cAcc c -> solveCongs cAcc c) (head cs) (tail cs)

smany = solveMany [c1, c2, c3]
-- Solution should be C 68 105


-- Now we need some code to transform the busids to a list of
-- congruences
id2Cong :: (Integer, Integer) -> Congruence
id2Cong (n, x') = C (mod (n - x') n) n

main = do
  contents <- readFile "input.txt"
  let [_, busstrs] = lines contents
      busids = parse busstrs
      congs = map id2Cong busids
      solution = solveMany congs

  print $ "I heard you liked modular arithmetic so I found this number: " ++ show (rhs solution)
  print "Merry Christmas across the land"




-- We are looking for a number x where mod x id0 = 0, mod (x+1) id1 = 0, mod (x+n) idn = 0, etc
-- Equivalently: mod x id0 = 0, id1 - mod x id1 = 1, idn - mod x idn = n, etc
-- So we know that:
-- x = id0*n0
-- x+1 = id1*n1
-- x+k = idk*nk
-- Want to solve equation id0*n0 = id1*n1 - 1 = idk*nk - k = ...
-- for integers n_i
-- Could do it as a sequence of filters:
-- Set0 = [id0 * n | n <- [1..]]
-- Set1 = filter (\x -> mod (x+1) id1 == 0) Set0
-- Setk = filter (\x -> mod (x+k) idk == 0) Set1
-- ...
-- But this approach is equivalent to checking each integer, probably not good

-- filterId (idk, k) = filter (\x -> mod (x+k) idk == 0)

-- This must exist already, otherwise could generalize by letting
-- idks actually be a list of filters or predicates
-- sequenceFilters startLs idks = foldl (\acc idk -> filterId idk acc) startLs idks
