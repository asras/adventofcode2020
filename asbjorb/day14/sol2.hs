import System.IO
import qualified Data.Map as M
import Data.Map (Map)
import Text.Read (readMaybe)
import Data.List.Split (splitOn)

-- Use map to represent non-empty memory addresses
-- Need functions to convert to an from 36 bit binary representation
-- We need to decide on a representation for binary numbers.
-- We want to do elementwise operations, so let's store it as a list

type Binary = [Int]

dec2Bin :: Int -> Binary
dec2Bin n = map (\i -> div (mod n (2^(i+1)) - mod n (2^i)) (2^i)) [35,34..0]

bin2Dec :: Binary -> Int
bin2Dec b = sum $ map (\(index, val) -> val * 2^index) $ zip [35,34..0] b

-- X now stands for Floating rather than Defer
data BitMask = One | Zero | Floating deriving Show

-- Applying a mask now yields a list of Binary addresses
appendToAll listOfLists el = map (\ls -> el:ls) listOfLists
appendAllToAll listOfEls listOfLists = listOfEls >>= appendToAll listOfLists

applyMask :: [BitMask] -> Binary -> [Binary]
applyMask m b = foldr (\t acc -> masker t acc) [[]] (zip m b)
  where
    masker (One, bit) ls = appendAllToAll [1] ls
    masker (Zero, bit) ls = appendAllToAll [bit] ls
    masker (Floating, bit) ls = appendAllToAll [0, 1] ls


testMask = [Floating | _ <- [0..35]]
testMask2 = [Zero | _ <- [0..35]]
testMask3 = Floating:[One | _ <- [0..34]]
b1 = dec2Bin 1202



parseMask :: String -> [BitMask]
parseMask = map parser
  where
    parser '1' = One
    parser '0' = Zero
    parser 'X' = Floating
    parser x = error ("Got: " ++ show x)


-- Parse memory string to location, value tuple
parseMem :: String -> (Int, Int)
parseMem s = (read $ filter isNum p1, read p2)
  where
    [p1, p2] = splitOn " = " s
    isNum :: Char -> Bool
    isNum c = case readMaybe [c] :: Maybe Int of
                Just _ -> True
                Nothing -> False

-- Now we need a description of the program
data Instruction = AlterMask [BitMask] | AssignMem (Int, Int) deriving Show

-- Then a function to parse a line of input
parseInput :: String -> Instruction
parseInput s = let [p1, p2] = splitOn " = " s in
                 if (p1 == "mask")
                 then AlterMask (parseMask p2)
                 else AssignMem (parseMem s)

-- A description of the program state
-- First is mask, second is memory values
type State = ([BitMask], Map Int Int)

-- Now we need functions to run the program
run :: Instruction -> State -> State
run (AlterMask m) (mask, mem) = (m, mem)
run (AssignMem (a, b)) (mask, mem) = (mask, foldl (\m add -> M.insert add b m) mem addresses)
  where
    addresses = map bin2Dec $ applyMask mask $ dec2Bin a

main = do
  contents <- lines <$> readFile "input.txt"
  let p1 = parseInput $ head contents
      instructions = map parseInput contents
      startState = ([Zero | _ <- [0..35]], M.empty)
      finalState = foldl (\state ins -> run ins state) startState instructions
      sumMem = sum $ map (\k -> (snd finalState) M.! k) $ M.keys (snd finalState) 

  print $ "I'm a computer: " ++ (show sumMem)
  print "Christmas time is coming!"
