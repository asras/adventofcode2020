{-# LANGUAGE BangPatterns #-}
import System.IO
import qualified Data.Map as M
import Data.Map (Map)
import Data.List (foldl')

-- Because we need to go to huge turn numbers now,
-- we should not store all the previous times a number was spoken

-- Increment all values by 1
incVals :: Map Integer Integer -> Map Integer Integer
incVals m = M.map (+1) m


-- Starting vals
starts = [12, 20, 0, 6, 1, 17, 7]
-- starts = [0, 3, 6]

-- A Game is a turn number, what was said in the previous turn
-- and a 'memory' i.e. record of what has been said
-- and when,
type Game = (Integer, Integer, Map Integer [Integer])

startMap = M.fromList $ map (\(a, b) -> (a, [b])) $ zip starts [1..] :: Map Integer [Integer]
startGame = (toInteger $ length starts + 1, last starts, startMap) :: Game -- Time to start

turn :: Game -> Game
turn (num, !pv, !mem) = (num + 1, v, newmem)
  where
    -- Insert new saying time into memory
    newmem = if M.member v mem
                then M.insert v [num, (head $ mem M.! v)] mem
                else M.insert v [num] mem
    -- And now we find out what should be said
    v = if M.member pv mem && length (mem M.! pv) > 1
           then let sayings = mem M.! pv in (head sayings) - (head $ tail sayings)
           else 0

-- Run the game until we are at turn n+1 and print last number said
-- i.e. number said at turn n
haveFun :: Int -> Game -> Integer
haveFun n game = sndof3 $ endGame
  where
    sndof3 = (\(a,b,c) -> b) -- I know theres a func for this somewhere
    endGame = foldl' (\g _ -> turn g) game [length starts + 1..n]
                                                                        

haveFun' n game = head $ drop n $ iterate turn game


main = do
  print $ "This year I will get: " ++ (show $ haveFun 30000000 startGame) ++ " presents!"
