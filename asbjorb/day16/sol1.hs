import System.IO
import Data.List.Split (splitOn)


type Constraint = ((Int, Int), (Int, Int))
type Ticket = [Int]

checkRange :: Constraint -> Int -> Bool
checkRange ((l1, u1), (l2, u2)) x = (x >= l1 && x <= u1) || (x >= l2 && x <= u2)

checkField :: Int -> [Constraint] -> Bool
checkField f cs = any (flip checkRange f) cs

-- Check for fields that do not satisfy any constraints
invalidFields :: [Constraint] -> Ticket -> [Int]
invalidFields cs fs = filter (\f -> not $ checkField f cs) fs

readConstraint :: String -> Constraint
readConstraint s = parseParts $ map (splitOn "-") $ splitOn " or " $ (splitOn ": " s) !! 1
  where
    parseParts [[a1, b1], [a2, b2]] = ((read a1, read b1), (read a2, read b2))

readTicket :: String -> Ticket
readTicket s = map read $ splitOn "," s

main = do
  [constraints, mytick, tickets] <- splitOn "\n\n" <$> readFile "input.txt"
  let cs = map readConstraint $ lines constraints
      ts = map readTicket $ tail $ lines tickets
      badFields = map (invalidFields cs) ts

  print $ "Error Error: " ++ (show $ foldl (\acc ls -> acc + sum ls) 0 badFields)
  print $ "All I want for christmas is a few tens of millions of dollars"
