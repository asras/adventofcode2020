import System.IO
import Data.List.Split (splitOn)
import Data.List (foldl', sortBy, isPrefixOf)

-- Constraints now need a name
-- We rename type to Field
type Field = (String, (Int, Int), (Int, Int))
type Ticket = [Int]

checkRange :: Field -> Int -> Bool
checkRange (_, (l1, u1), (l2, u2)) x = (x >= l1 && x <= u1) || (x >= l2 && x <= u2)

checkEntry :: Int -> [Field] -> Bool
checkEntry f cs = any (flip checkRange f) cs

-- Check for fields that do not satisfy any constraints
invalidFields :: [Field] -> Ticket -> [Int]
invalidFields cs fs = filter (\f -> not $ checkEntry f cs) fs


-- Possibilities will be list of length N_fields
-- where each element is a list of possible fields it can be
-- We then implement an algorithm that goes through a list of tickets
-- and determines what the updated possibilities should be
type Possibilities = [[Field]]


-- We update possibilities by checking each of the current
-- possibilities against each field.
-- Those that invalidate the field are discarded.
updatePoss :: Possibilities -> Ticket -> Possibilities
updatePoss poss tick | length poss /= length tick = error "Unequal length"
updatePoss poss tick = map eliminateInvalidators $ zip poss tick
  where
    eliminateInvalidators (fields, entry) = filter (\f -> checkRange f entry) fields

-- We need a function to combine the list of possibilities
-- We have an accumulator list as first arg
-- The enumerated possibilities as second
-- We assume the enumerated possibilities are ordered by length
-- We hope there is a unique choice at each step
foldPoss :: [(Int, Field)] -> [(Int, [Field])] -> [(Int, Field)]
foldPoss acc [] = acc
foldPoss acc ((i, x):xs) = foldPoss acc' xs
  where
    options = filter (\f -> not $ elem f $ map snd acc) x
    acc' = case length options of
             1 -> (i, head options):acc
             _ -> error "So I have to be smart?"

readField :: String -> Field
readField s = (name, fstrange, sndrange)
  where
    parts = (splitOn ": " s)
    name = parts !! 0
    (fstrange, sndrange) = parseParts $ map (splitOn "-") $ splitOn " or " $ parts !! 1
    parseParts [[a1, b1], [a2, b2]] = ((read a1, read b1), (read a2, read b2))
    
readTicket :: String -> Ticket
readTicket s = map read $ splitOn "," s


main = do
  [constraints, mytick, tickets] <- splitOn "\n\n" <$> readFile "input.txt"
  let cs = map readField $ lines constraints
      ts = map readTicket $ tail $ lines tickets
      myt = readTicket $ (lines mytick) !! 1
      goodTicks = filter (\t -> length (invalidFields cs t) == 0) ts

      -- Before we eliminate any possibilities everything is
      -- possible. The sky is the limit. Follow your dreams.
      -- Don't let anyone tell you otherwise.
      startPossi = [cs | _ <- [1..length myt]]

      -- Run through all nearby tickets to narrow down possiblities
      endPoss = foldl' (\p t -> updatePoss p t) startPossi goodTicks

      -- Enumerate narrowed possibilities so we don't lose information
      -- on which field is which. Then order by length of
      -- possibilities. Here we are hoping that some entry will
      -- have only one possible field, and any field will
      -- be completely by the previous fields.
      enumeratedPos = zip [0..] endPoss
      sortLen = \t1 t2-> if length (snd t1) > length (snd t2) then GT else LT
      
      -- Then we go through list of possibilities and select the
      -- only one possible
      reducedPoss = foldPoss [] $ sortBy sortLen enumeratedPos

      -- Select indices of those fields that start with the word departure
      fst3 = \(a, b, c) -> a
      selectName = fst3 . snd
      departureFields = filter (\t -> "departure" `isPrefixOf` (selectName t)) reducedPoss
      indices = map fst departureFields
      
  print $ product $ [myt !! i | i <- indices]
  print $ "All I want for christmas is a few tens of millions of dollars"
