import System.IO
import Data.List (nub)

type State = [(Int, Int, Int)] -- List of live positions

neighbors :: (Int, Int, Int) -> [(Int, Int, Int)]
neighbors (x, y, z) = [(x+xi, y+yi, z+zi) | xi <- [-1..1], yi <-[-1..1], zi <- [-1..1],
                       not (xi == 0 && yi == 0 && zi == 0)]

                      
inactiveToCheck state = filter (\i -> not $ elem i state) $ nub $ (state >>= neighbors)

liveNeighs x state = length $ filter (\ind -> elem ind state) (neighbors x)

updateState :: State -> State
updateState state = (filter inactiveLive (inactiveToCheck state)) ++ (filter activeLive activeToCheck)
  where
    activeLive (a, b, c) = case liveNeighs (a, b, c) state of
                             2 -> True
                             3 -> True
                             _ -> False
    inactiveLive (a, b, c) = case liveNeighs (a, b, c) state of
                               3 -> True
                               _ -> False
    activeToCheck = state
    
readStart :: [String] -> State
readStart s = [(i, j, 0) | i <- [0..(length s - 1)], j <- [0..(length (s!!0) -1)], (s !! i) !! j == '#']


s0 = [(0, 1, 0), (1, 2, 0), (2, 0, 0), (2, 1, 0), (2, 2, 0)] :: [(Int, Int, Int)]

main = do
  contents <- lines <$> readFile "input.txt"
  let state = readStart contents
      c1 = updateState state
      c2 = updateState c1
      fi = map length $ take 7 $ iterate updateState state

  print $ fi
  print "Freshly cut Christmas trees smelling of stars and snow and pine resin - inhale deeply and fill your soul with wintry night."
