{-# LANGUAGE FlexibleContexts #-}
import System.IO
import Data.List (nub)
--import Data.Vector (Vector)
--import qualified Data.Vector as V
import Data.Vector.Unboxed (Vector)
import qualified Data.Vector.Unboxed as V

type Pos = (Int, Int, Int, Int)
type State = Vector Pos -- List of live positions

neighbors :: Pos -> State
neighbors (x, y, z, w) = V.fromList [(x+xi, y+yi, z+zi, w + wi)
                                    | xi <- [-1..1], yi <-[-1..1], zi <- [-1..1], wi <- [-1..1],
                                      not (xi == 0 && yi == 0 && zi == 0 && wi == 0)]


toCheck :: Vector Pos -> Vector Pos
toCheck state = V.fromList [(x, y, z, w) | x<-[minx-1..maxx+1], y<-[miny-1..maxy+1], z<-[minz-1..maxz+1], w<-[minw-1..maxw+1]]
  where
    minx = V.minimum $ V.map (\(a,b,c,d) -> a) state
    maxx = V.maximum $ V.map (\(a,b,c,d) -> a) state
    miny = V.minimum $ V.map (\(a,b,c,d) -> b) state
    maxy = V.maximum $ V.map (\(a,b,c,d) -> b) state
    minz = V.minimum $ V.map (\(a,b,c,d) -> c) state
    maxz = V.maximum $ V.map (\(a,b,c,d) -> c) state
    minw = V.minimum $ V.map (\(a,b,c,d) -> d) state
    maxw = V.maximum $ V.map (\(a,b,c,d) -> d) state


                         
inactiveToCheck state = V.filter (\x -> not $ V.elem x state) $ V.fromList $ nub $ V.toList $ V.concatMap neighbors state

neighbors' (x, y, z, w) = [(x+xi, y+yi, z+zi, w + wi)
                          | xi <- [-1..1], yi <-[-1..1], zi <- [-1..1], wi <- [-1..1],
                            not (xi == 0 && yi == 0 && zi == 0 && wi == 0)]

inactiveToCheck' state = filter (\i -> not $ elem i state) $ nub $ (concat $ map neighbors' state)

--  filter (\i -> not $ V.elem i state) $ nub $ (V.toList state >>= neighbors)

liveNeighs x state = V.length $ V.filter (\ind -> V.elem ind state) (neighbors x)

updateState :: State -> State
updateState state = V.filter isLive indices
  where
    indices = toCheck state
    isLive x = if V.elem x state
                  then activeLive x
                  else inactiveLive x
    
    activeLive x = case liveNeighs x state of
                             2 -> True
                             3 -> True
                             _ -> False
    inactiveLive x = case liveNeighs x state of
                               3 -> True
                               _ -> False
    -- activeToCheck = V.toList state
    --  V.fromList $  (filter inactiveLive (inactiveToCheck state)) ++ (filter activeLive activeToCheck)

    
readStart :: [String] -> State
readStart s = V.fromList [(i, j, 0, 0) | i <- [0..(length s - 1)], j <- [0..(length (s!!0) -1)], (s !! i) !! j == '#']


s0 = V.fromList [(0, 1, 0, 0), (1, 2, 0, 0),
      (2, 0, 0, 0), (2, 1, 0, 0), (2, 2, 0, 0)] :: State

main = do
  contents <- lines <$> readFile "smallinput.txt"
  let state = readStart contents
      fi = map V.length $ take 7 $ iterate updateState state
      c1 = updateState state
      c2 = updateState c1
      c3 = updateState c2
      c4 = updateState c3
      c5 = updateState c4

  print $ V.length $ toCheck c1
  print $ V.length $ toCheck c2
  print $ V.length $ toCheck c3
  print $ V.length $ toCheck c4
  print $ V.length $ toCheck c5
  -- print fi
  print "Freshly cut Christmas trees smelling of stars and snow and pine resin - inhale deeply and fill your soul with wintry night."
