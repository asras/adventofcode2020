import System.IO
import Data.Set (Set)
import qualified Data.Set as S
import Data.Maybe (mapMaybe)
-- import Data.Map (Map)
--import qualified Data.Map as M
--import Data.Maybe (mapMaybe)

type State = Set (Int, Int, Int, Int)

-- neighbors :: Pos -> State
neighbors (x, y, z, w) = [(x+xi, y+yi, z+zi, w + wi)
                         | xi <- [-1..1], yi <-[-1..1], zi <- [-1..1], wi <- [-1..1],
                           not (xi == 0 && yi == 0 && zi == 0 && wi == 0)]

countLiveNeighs x state = sumBools $ map (\i -> S.member i state) $ neighbors x
  where
    sumBools bs = sum $ map (\b -> if b then 1 else 0) bs

updateState :: State -> State
updateState state = state'
  where
    (living, dying) = S.partition (\x -> let n = countLiveNeighs x state in n == 2 || n == 3) state
    birthing = S.fromList $ filter (\x -> countLiveNeighs x state == 3) inactiveToCheck
    inactiveToCheck = concat $ map neighbors $ S.toList state
    state' = S.union living birthing


readStart :: [String] -> State
readStart s = S.fromList [(i, j, 0, 0) | i <- [0..(length s - 1)], j <- [0..(length (s!!0) -1)], (s !! i) !! j == '#']


s0 = S.fromList $ [(0, 1, 0, 0), (1, 2, 0, 0),
                    (2, 0, 0, 0), (2, 1, 0, 0), (2, 2, 0, 0)] :: State

main = do
  contents <- lines <$> readFile "input.txt"
  let state = readStart contents
      fi = map S.size $ take 7 $ iterate updateState state
      
  print fi
  print "Freshly cut Christmas trees smelling of stars and snow and pine resin - inhale deeply and fill your soul with wintry night."

