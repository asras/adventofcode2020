import System.IO
import Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe (mapMaybe)

-- We switch to a map Map Position Int
-- The Key/position is as before the position in the grid
-- The Value is now used to apply the update rule in an efficient
-- manner. We loop through the keys in the map and update all
-- the neighboring keys, then use the values stored at a given key
-- to determine if the cell is alive or dead. Details:

-- Updated algo with time complexity
-- O(n) * 81 * O(log n) + n' * O(log n) \approx O(n * log n)
-- 1. Loop over all elements (that gives O(n))
-- 2. For each element, look at all neighbors including self (81 in 4D)
-- 3. Update the values in the dict at each of the neighbors (O(log n))
-- The cell adds 1 to the value/count of each neighbor and 100 to itself (100 > 81)
-- After looping through all neighbors, dead cells will have value/count equal
-- to the number of neighboring alive cells. Alive cells will have count equal to
-- 100 + number of neighboring alive cells. Because (100 > 81) we will always
-- be able to distinguish which cells were alive and which were dead before the loop.
-- 4. Loop through all cells in the updated dict and determine if they are now alive.
-- In the updated dict there are n' cells. This is in the worst case equal to 81 * n but
-- generally n' < 81 * n because two or more cells may have overlapping neighborhoods.
-- Inserting new keys into the dict is O(log n) which gives the final time complexity.




type State = Map (Int, Int, Int, Int) Int

-- neighbors :: Pos -> State
neighbors (x, y, z, w) = [(x+xi, y+yi, z+zi, w + wi)
                         | xi <- [-1..1], yi <-[-1..1], zi <- [-1..1], wi <- [-1..1],
                           not (xi == 0 && yi == 0 && zi == 0 && wi == 0)]

-- This is steps 1-3 in above description
updateCounts :: State -> State
updateCounts state = foldr (\key accmap -> insertCounts key accmap) M.empty $ M.keys state
  where
    insert100ToSelf k m = M.insertWith' (+) k 100 m
    insert1ToNeighs k m = foldr (\k' acc -> M.insertWith' (+) k' 1 acc) m $ neighbors k
    insertCounts k m = insert1ToNeighs k (insert100ToSelf k m)

determineLives :: State -> State
determineLives state = foldr (\key accmap -> M.update isAlive key accmap) state $ M.keys state
  where
    -- If isAlive returns a Just the cell is alive
    -- If it returns Nothing, key is deleted.
    -- Value doesnt matter
    isAlive x
      | x == 103  = Just 0
      | x == 102  = Just 0
      | x == 3    = Just 0
      | otherwise = Nothing


updateState :: State -> State
updateState = determineLives . updateCounts


readStart :: [String] -> State
readStart s = M.fromList $ map (\a -> (a, 0)) $ [(i, j, 0, 0) | i <- [0..(length s - 1)], j <- [0..(length (s!!0) -1)], (s !! i) !! j == '#']


s0 = M.fromList $ map (\a -> (a, 0)) $  [(0, 1, 0, 0), (1, 2, 0, 0),
                    (2, 0, 0, 0), (2, 1, 0, 0), (2, 2, 0, 0)] :: State

main = do
  contents <- lines <$> readFile "input.txt"
  let state = readStart contents
      final = take 7 $ iterate updateState state

  print $ map M.size final
  print "Freshly cut Christmas trees smelling of stars and snow and pine resin - inhale deeply and fill your soul with wintry night."

