import System.IO
import Text.Read (readMaybe)

isnum c = elem c ['0'..'9']

-- Take whats inside a possibly nested parenthesis: ((a +d a+wd a) +wad a d)
takeUntilClosed :: Int -> String -> String -> (String, String)
takeUntilClosed 0 acc ls = (reverse $ tail $ reverse $ acc, ls)
takeUntilClosed x acc (')':ls) = takeUntilClosed (x-1) (acc++")") ls
takeUntilClosed x acc ('(':ls) = takeUntilClosed (x+1) (acc++"(") ls
takeUntilClosed x acc (l:ls) = takeUntilClosed (x) (acc++[l]) ls


data Expr = Number Int | SumOp | ProdOp | Plus Int | Prod Int deriving Show

parseSubExpr :: String -> (String, Expr)
parseSubExpr ('(':cs) = (rest, consumed)
  where
    (start, rest) = takeUntilClosed 1 "" cs
    (unconsumed, consumed) = parse (start, Number 0)

combine :: Expr -> Expr -> Expr
combine (Number x) (Number y) = Number (x*10 + y)
combine (Number x) (SumOp) = Plus x
combine (Plus x) (Number y) = Number (x + y)
combine (Number x) (ProdOp) = Prod x
combine (Prod x) (Number y) = Number (x * y)

parse :: (String, Expr) -> (String, Expr)
parse ("", expr) = ("", expr)
parse ((c:cs), expr)
  | c == '+' = parse (cs, combine expr SumOp)
  | c == '*' = parse (cs, combine expr ProdOp)
  | isnum c = parse (cs, combine expr (Number (read [c])))
  | c == '(' = parse $ (rest, combine expr (parsedSub))
  where
    (rest, parsedSub) = parseSubExpr (c:cs)

t1 = filter (\c -> c /= ' ') "1 + 2 * 3 + 4 * 5 + 6"
t2 = filter (\c -> c /= ' ') "2 * 3 + (4 * 5)"


eval (Number x) = x

main = do
  exprs <- lines <$> readFile "input.txt"
  let values = map (eval . snd) $ map (\s -> parse (s, Number 0)) $ map (filter (\c -> c /= ' ')) exprs


  print $ sum values
  print "Christmas is a necessity."
