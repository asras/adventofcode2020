import System.IO
import Text.Read (readMaybe)
import Data.List.Split (splitOn)
import Data.List (groupBy)
import Data.Maybe (mapMaybe)

takeUntilClosed :: Int -> String -> String -> (String, String)
takeUntilClosed 0 acc ls = (reverse $ tail $ reverse $ acc, ls)
takeUntilClosed x acc (')':ls) = takeUntilClosed (x-1) (acc++")") ls
takeUntilClosed x acc ('(':ls) = takeUntilClosed (x+1) (acc++"(") ls
takeUntilClosed x acc (l:ls) = takeUntilClosed (x) (acc++[l]) ls


isnum c = elem c ['0'..'9']

-- A datatype that basically just contains the string
-- This way we can use typing to control computation
data Token = TNum Int | TOp Char | TSubExpr String deriving Show

-- Token converters Token -> Bool and Token -> Maybe Number
isplus (TOp '+') = True
isplus _ = False
isprod (TOp '*') = True
isprod _ = False
issub (TSubExpr _) = True
issub _ = False
unnum (TNum x) = Just x
unnum _ = Nothing


-- Tokenizers
tokNum :: String -> (String, Maybe Token)
tokNum s = (rest, tok)
  where
    tok = case (readMaybe (takeWhile isnum s) :: Maybe Int) of
            Just x -> Just (TNum x)
            Nothing -> Nothing
    rest = dropWhile isnum s

tokSubExpr :: String -> (String, Maybe Token)
tokSubExpr ('(':s) = (rest, Just sub)
  where
    (sub', rest) = takeUntilClosed 1 "" s
    sub = TSubExpr sub'
tokSubExpr s = (s, Nothing)

tokOp :: String -> (String, Maybe Token)
tokOp ('+':s) = (s, Just (TOp '+'))
tokOp ('*':s) = (s, Just (TOp '*'))
tokOp s = (s, Nothing)

-- /Tokenizers


-- Apply all parsers to start of list, take first one that is successful
parseStep s = head $ filter notnothing $ map ($s) [tokNum, tokOp, tokSubExpr]
  where
    notnothing (_, Nothing) = False
    notnothing (_, Just _) = True

-- Chain parsing steps to accumulate a list of parsed tokens
parse s = while (\x -> length x /= 0) parseStep (s, [])
  where
    while p f (s', x)
      | p s' = while p f (fst $ f s', (snd $ f s'):x)
      | otherwise = mapMaybe id $ reverse x


firstIndex p [] = -1
firstIndex p ls = fst $ head $ filter (\(a,b) -> p b) $ zip [0..] ls

-- Because plusses have precedence we can evaluate them
-- and replace the Tokens by a Number-token
-- e.g. [Token 2, Token *, Token 3, Token +, Token 4]
-- becomes [Token 2, Token *, Token 7]
evalPlusses tokens = while (\ts -> any isplus ts) plusit tokens
  where
    while b f x
      | b x = while b f (f x)
      | otherwise = x

    plusit ts = [t | (i, t) <- (zip [0..] ts), i < (index - 1)] ++ [newnum] ++ [t | (i, t) <- (zip [0..] ts), i > (index + 1)]
      where
        index = firstIndex isplus ts
        lhs = ts !! (index - 1)
        rhs = ts !! (index + 1)
        newnum = TNum ((evalS lhs) + (evalS rhs))
        
-- Subroutine for evaluation
evalS (TNum x) = x
evalS (TSubExpr x) = eval (parse x)

-- Main evaluator
eval tokens
  | any issub tokens = eval [if issub t then (TNum (evalS t)) else t | t <- tokens]
  | any isplus tokens = eval $ evalPlusses tokens
  | otherwise = product $ mapMaybe unnum tokens



t1 = filter (\c -> c /= ' ') "1 + (2 * 3) + (4 * (5 + 6))"
t2 = filter (\c -> c /= ' ') "2 * 3 + (4 * 5)"
t3 = filter (\c -> c /= ' ') "1 + 2 * 3 + 4 * 5 + 6"
t4 = filter (\c -> c /= ' ') "6 * ((4 * 8 + 4) + (3 * 5 + 3 + 3 * 7 * 5)) + 7 + (9 * (6 + 9 * 7 + 2 * 6 * 6)) + 2"


main = do
  contents <- lines <$> readFile "input.txt"
  let tokenlines = map parse $ map (filter (\c -> c /= ' ')) contents
      values = map eval tokenlines
      
  print $ sum values
  print "Christmas is a necessity."
