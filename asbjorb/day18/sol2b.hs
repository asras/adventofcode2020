import System.IO
import Data.Maybe (mapMaybe)
import Text.Read (readMaybe)


takeUntilClosed :: Int -> String -> String -> (String, String)
takeUntilClosed 0 acc ls = (reverse $ tail $ reverse $ acc, ls)
takeUntilClosed x acc (')':ls) = takeUntilClosed (x-1) (acc++")") ls
takeUntilClosed x acc ('(':ls) = takeUntilClosed (x+1) (acc++"(") ls
takeUntilClosed x acc (l:ls) = takeUntilClosed (x) (acc++[l]) ls

data Token = TNum Int | TOp Char | TSubExpr String | TExpr Token Token Token deriving Show
isplus (TOp '+') = True
isplus _ = False
isprod (TOp '*') = True
isprod _ = False
issub (TSubExpr _) = True
issub _ = False
unnum (TNum x) = Just x
unnum _ = Nothing


isnum c = elem c ['0'..'9']
-- Tokenizers
tokNum :: String -> (String, Maybe Token)
tokNum s = (rest, tok)
  where
    tok = case (readMaybe (takeWhile isnum s) :: Maybe Int) of
            Just x -> Just (TNum x)
            Nothing -> Nothing
    rest = dropWhile isnum s

tokSubExpr :: String -> (String, Maybe Token)
tokSubExpr ('(':s) = (rest, Just sub)
  where
    (sub', rest) = takeUntilClosed 1 "" s
    sub = TSubExpr sub'
tokSubExpr s = (s, Nothing)

tokOp :: String -> (String, Maybe Token)
tokOp ('+':s) = (s, Just (TOp '+'))
tokOp ('*':s) = (s, Just (TOp '*'))
tokOp s = (s, Nothing)
-- /Tokenizers
-- Apply all parsers to start of list, take first one that is successful
parseStep s = head $ filter notnothing $ map ($s) [tokNum, tokOp, tokSubExpr]
  where
    notnothing (_, Nothing) = False
    notnothing (_, Just _) = True

-- Chain parsing steps to accumulate a list of parsed tokens
parse s = while (\x -> length x /= 0) parseStep (s, [])
  where
    while p f (s', x)
      | p s' = while p f (fst $ f s', (snd $ f s'):x)
      | otherwise = mapMaybe id $ reverse x


firstIndex p [] = -1
firstIndex p ls = fst $ head $ filter (\(a,b) -> p b) $ zip [0..] ls

condense p ts = before ++ [TExpr optoken lhstok rhstok] ++ after
  where
    index = firstIndex p ts
    before = [t | (i, t) <- (zip [0..] ts), i < index - 1]
    optoken = ts !! index
    lhstok = ts !! (index - 1)
    rhstok = ts !! (index + 1)
    after = [t | (i, t) <- (zip [0..] ts), i > index + 1]


expandASub ts = before ++ (toTree $ parse expr) ++ after
  where
    index = firstIndex issub ts
    before = [t | (i, t) <- (zip [0..] ts), i < index]
    after = [t | (i, t) <- (zip [0..] ts), i > index]
    (TSubExpr expr) = (ts !! index)

-- condenseAProd ts = before ++ [TExpr optoken lhstok rhstok] ++ after
--   where
--     index = firstIndex isprod ts
--     before = [t | (i, t) <- (zip [0..] ts), i < index - 1]
--     optoken = ts !! index
--     lhstok = ts !! (index - 1)
--     rhstok = ts !! (index + 1)
--     after = [t | (i, t) <- (zip [0..] ts), i > index + 1]


toTree tokens
  | any issub tokens = toTree $ expandASub tokens
  | any isplus tokens = toTree $ condense isplus tokens
  | any isprod tokens = toTree $ condense isprod tokens
  | otherwise = tokens


eval' (TExpr (TOp '*') t2 t3) = (eval' t2) * (eval' t3)
eval' (TExpr (TOp '+') t2 t3) = (eval' t2) + (eval' t3)
eval' (TNum x) = x

eval ts = eval' (head ts)


t1 = filter (\c -> c /= ' ') "1 + (2 * 3) + (4 * (5 + 6))"
t2 = filter (\c -> c /= ' ') "2 * 3 + (4 * 5)"
t3 = filter (\c -> c /= ' ') "1 + 2 * 3 + 4 * 5 + 6"
t4 = filter (\c -> c /= ' ') "6 * ((4 * 8 + 4) + (3 * 5 + 3 + 3 * 7 * 5)) + 7 + (9 * (6 + 9 * 7 + 2 * 6 * 6)) + 2"


main = do
  contents <- lines <$> readFile "input.txt"
  let tokenlines = map parse $ map (filter (\c -> c /= ' ')) contents
      values = map (eval . toTree) tokenlines

  print $ sum values

  print "Christmas is a necessity"
