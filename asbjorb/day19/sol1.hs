import System.IO
import Data.Map (Map)
import qualified Data.Map as M
import Data.List.Split (splitOn)
import Control.Monad (foldM)
import Data.Maybe (mapMaybe)

headMaybe [] = Nothing
headMaybe (x:xs) = Just x

-- A rule is either matching a char (Char-Rule)
-- or matching a list of rules (in order) (List-Rule)
-- or matching one of two lists of rules (Or-Rule)
-- data Rule = CRule Char | LRule [Rule] | ORule Rule Rule deriving Show
-- Actually we want to put the rules in a Map so we use a simpler datatype:
data Rule = CRule Char | LRule [Int] | ORule [Int] [Int] deriving Show


-- Check rule. Return Nothing is check fails
-- Return Just rest if it succeeds where rest is
-- the remainder of the string
checkRule :: Map Int Rule -> String -> Rule -> Maybe String
checkRule _ s (CRule char) = if length s > 0 && head s == char then Just (tail s) else Nothing
checkRule rules s (LRule others) = foldM (\acc i -> checkRule rules acc (rules M.! i)) s others
checkRule rules s (ORule o1 o2) = headMaybe $ mapMaybe (\ls -> checkRule rules s (LRule ls)) [o1, o2]


isokletter c = elem c ([' ', '"']++['a'..'z'])
isletter c = elem c ['a'..'z']


parseRules :: String -> (Map Int Rule, [String])
parseRules s = (foldl (\accmap line -> addLine line accmap) M.empty (lines s'), lines inputs)
  where
    [s', inputs] = splitOn "\n\n" s
    addLine line accmap = M.insert key rule accmap
      where
        [key', rest] = splitOn ": " line
        key = read key'
        rule = if all isokletter rest
                  then CRule (head $ filter isletter rest)
                  else if elem '|' rest
                          then oruleParse rest
                          else LRule (listParse rest)
        oruleParse rest = let [p1, p2] = splitOn " | " rest in ORule (listParse p1) (listParse p2)
        listParse rest = map read $ splitOn " " rest


main = do
  (rules, inputs) <- parseRules <$> readFile "input.txt"
  let rule0 = rules M.! 0

  print $ length $ filter ((==0) . length) $ mapMaybe (\input -> checkRule rules input rule0) inputs

  print "The best way to spread Christmas cheer is singing loud for all to hear."
