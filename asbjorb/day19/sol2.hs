import System.IO
import Data.Map (Map)
import qualified Data.Map as M
import Data.List.Split (splitOn)
import Control.Monad (foldM)
import Data.Maybe (mapMaybe)


headMaybe [] = Nothing
headMaybe (x:xs) = Just x

data Rule = CRule Char | LRule [Int] | ORule [Int] [Int] deriving Show

-- Check rule. Return Nothing is check fails
-- Return Just rest if it succeeds where rest is
-- the remainder of the string
checkRule :: Map Int Rule -> String -> Rule -> Maybe String
checkRule _ s (CRule char) = if length s > 0 && head s == char then Just (tail s) else Nothing
checkRule rules s (LRule others) = foldM (\acc i -> checkRule rules acc (rules M.! i)) s others
checkRule rules s (ORule o1 o2) = headMaybe $ mapMaybe (\ls -> checkRule rules s (LRule ls)) [o1, o2]


isokletter c = elem c ([' ', '"']++['a'..'z'])
isletter c = elem c ['a'..'z']


parseRules :: String -> (Map Int Rule, [String])
parseRules s = (foldl (\accmap line -> addLine line accmap) M.empty (lines s'), lines inputs)
  where
    [s', inputs] = splitOn "\n\n" s
    addLine line accmap = M.insert key rule accmap
      where
        [key', rest] = splitOn ": " line
        key = read key'
        rule = if all isokletter rest
                  then CRule (head $ filter isletter rest)
                  else if elem '|' rest
                          then oruleParse rest
                          else LRule (listParse rest)
        oruleParse rest = let [p1, p2] = splitOn " | " rest in ORule (listParse p1) (listParse p2)
        listParse rest = map read $ splitOn " " rest


-- A function to return the part of the string
-- that matches the rule
getMatchingPart :: Map Int Rule -> String -> Rule -> Maybe String
getMatchingPart rules input rule = case checkRule rules input rule of
                                     Nothing -> Nothing
                                     Just x -> Just (listDiff input x)
                                     where
                                       listDiff input x = take (length input - length x) input


-- Rule 0 is now: Any number (but at least 1) of rule 42 followed by N rule 42 followed by N rule 31
-- Find the part that matches rule 42
-- Then drop all those, find the part that matches rule 31
-- Check that N_42 > N_31 >= 1
checkNewRule0 :: Map Int Rule -> String -> Bool
checkNewRule0 rules input = case do
  (n42, rest) <- dropWhileMatch rules input (rules M.! 42)
  (n31, rest') <- dropWhileMatch rules rest (rules M.! 31)
  if length rest' == 0 then Just 0 else Nothing -- Must be a better way
  return (n42, n31)
  of
  Nothing -> False
  Just (n42, n31) -> n42 > n31 && n31 >= 1


dropWhileMatch :: Map Int Rule -> String -> Rule -> Maybe (Integer, String)
dropWhileMatch rules s rule = while isjust (\x -> checkRule rules x rule) 0 (Just s)
  where
    while p f i x
      | p (x >>= f) = while p f (i+1) (x >>= f)
      | otherwise = if i == 0 then Nothing else rejust (i, x)
    isjust x = case x of
                 Nothing -> False
                 Just _ -> True
    rejust (_, Nothing) = Nothing
    rejust (i, Just x) = Just (i, x)

main = do
  (rules, inputs) <- parseRules <$> readFile "input.txt"
  print $ length $ filter (checkNewRule0 rules) inputs
  print "The best way to spread Christmas cheer is singing loud for all to hear."
