import System.IO
import Data.List
import Data.List.Split
import Text.Read (readMaybe)

data Requirement = Req {minv :: Int,
                        maxv :: Int,
                        char :: Char} deriving Show
type Password = String

(<<=) = flip (>>=)

parts :: String -> String -> Maybe (String, String)
parts splitter s
  | length splitS == 2 = Just (splitS !! 0, splitS !! 1)
  | otherwise = Nothing
    where
      splitS = splitOn splitter s

readInt :: String -> Maybe Int
readInt = readMaybe

mapT f (a, b) = (f a, f b)
seqT :: (Maybe a, Maybe b) -> Maybe (a, b)
seqT (Just a, Just b) = Just (a, b)
seqT _ = Nothing

parseReq :: String -> Maybe Requirement
parseReq s = do
  (limits, key) <- parts " " s
  (minv, maxv) <- (seqT . mapT readInt) <<= parts "-" limits
  return (Req minv maxv (head key))


testreq = "1-3 a"
testpass = "abcde"
testinput = "1-3 a:abcde"

parseInput :: String -> Maybe (Requirement, Password)
parseInput s = do
  (reqstr, pass) <- parts ":" s
  req <- parseReq reqstr
  return (req, pass)

testData = Req 1 3 'a'

satisfiesReq :: Requirement -> Password -> Bool
satisfiesReq req pass = count >= (minv req) && count <= (maxv req)
  where
    count = length $ filter (\c -> c == (char req)) pass


countValid :: [(Requirement, Password)] -> Int
countValid = length . filter (\(a, b) -> satisfiesReq a b) 


parseContent :: String -> Maybe [(Requirement, Password)]
parseContent content = sequence . map parseInput $ lines content 

reportValid :: String -> String
reportValid content = case fmap countValid $ parseContent content of
                        Just i -> show i
                        Nothing -> "Parse error"




nonEmptyLines :: String -> [String]
nonEmptyLines contents = filter (\l -> length l > 0) $ lines contents
   
main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let msg = reportValid contents
      putStrLn msg
      putStrLn "Merry Christmas")

