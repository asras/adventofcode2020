import System.IO
import Data.List
import Data.List.Split
import Text.Read (readMaybe)
import Data.Char (isSpace)

data Requirement = Req {minv :: Int,
                        maxv :: Int,
                        char :: Char} deriving Show
type Password = String

(<<=) = flip (>>=)

parts :: String -> String -> Maybe (String, String)
parts splitter s
  | length splitS == 2 = Just (splitS !! 0, splitS !! 1)
  | otherwise = Nothing
    where
      splitS = map (dropWhile isSpace) $ splitOn splitter s -- Test more!

readInt :: String -> Maybe Int
readInt = readMaybe

mapT f (a, b) = (f a, f b)
seqT :: (Maybe a, Maybe b) -> Maybe (a, b)
seqT (Just a, Just b) = Just (a, b)
seqT _ = Nothing

parseReq :: String -> Maybe Requirement
parseReq s = do
  (limits, key) <- parts " " s
  (minv, maxv) <- (seqT . mapT readInt) <<= parts "-" limits
  return (Req minv maxv (head key))


testreq = "1-3 a"
testpass = "abcde"
testinput = "1-3 a:abcde"

parseInput :: String -> Maybe (Requirement, Password)
parseInput s = do
  (reqstr, pass) <- parts ":" s
  req <- parseReq reqstr
  return (req, pass)

testData = Req 1 3 'a'
treq = Req 1 3 'b'
tp = "cdefg"
treq2 = Req 2 9 'c'
tp2 = "cccccccccccccccccc"

satisfiesReq :: Requirement -> Password -> Bool
satisfiesReq req pass = (c1 + c2) == 1
  where
    pos1 = minv req - 1
    pos2 = maxv req - 1
    key = char req
    c1 = if (pass !! pos1) == key then 1 else 0
    c2 = if (pass !! pos2) == key then 1 else 0

countValid :: [(Requirement, Password)] -> Int
countValid = length . filter (\(a, b) -> satisfiesReq a b) 


parseContent :: String -> Maybe [(Requirement, Password)]
parseContent content = sequence . map parseInput $ lines content 

reportValid :: String -> String
reportValid content = case fmap countValid $ parseContent content of
                        Just i -> show i
                        Nothing -> "Parse error"



   
main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let msg = reportValid contents
      putStrLn msg
      putStrLn "Merry Christmas")

