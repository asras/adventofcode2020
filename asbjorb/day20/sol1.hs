import System.IO
import Data.List.Split (splitOn)
import Data.List
import Data.Maybe (mapMaybe)


fac 0 = 1
fac n = n * fac (n-1)
headMaybe [] = Nothing
headMaybe (x:xs) = Just x
dropLast = reverse . tail . reverse

data Tile = T {tileid :: Integer,
               left :: String, right :: String, top :: String, bottom :: String} deriving (Show, Eq)

parseTile :: String -> Tile
parseTile s = T tid lef rig to bot
  where
    parts' = lines s
    tid = read $ filter (\c -> elem c ['0'..'9']) (head parts')
    parts = tail parts'
    len = length parts
    lef = [(parts !! i) !! 0 | i <- [0..len-1]]
    rig = [(parts !! i) !! (len - 1) | i <- [0..len-1]]
    to = [(parts !! 0) !! i | i <- [0..len-1]]
    bot = [(parts !! (len-1)) !! i | i <- [0..len-1]]


-- Mirror in y-axis
flipHori :: Tile -> Tile
flipHori (T id left right top bottom) = T id right left (reverse top) (reverse bottom)

-- Mirror in x-axis
flipVert :: Tile -> Tile
flipVert (T id left right top bottom) = T id (reverse left) (reverse right) bottom top

-- Rotate 90 degrees counterclockwise
rot :: Tile -> Tile
rot (T id left right top bottom) = (T id (bottom) (top) (reverse left) (reverse right))

rotn 0 = id
rotn n = rot . rotn (n-1)

allVersions :: Tile -> [Tile]
allVersions t = map ($t) operations
  where
    operations = [rotn i | i <- [0..3]] ++ [flipHori . (rotn i) | i <- [0..3]]


data Edge = LE | RE | TE | BE | NE deriving Show

removeAt idx xs = lft ++ rgt
  where (lft, (_:rgt)) = splitAt idx xs


findMatches :: Edge -> Tile -> Tile -> [Tile]
findMatches NE t1 t2 = allVersions t2
findMatches LE t1 t2 = filter (\t -> (left t1) == (right t)) (allVersions t2)
findMatches RE t1 t2 = filter (\t -> (right t1) == (left t)) (allVersions t2)
findMatches TE t1 t2 = filter (\t -> (top t1) == (bottom t)) (allVersions t2)
findMatches BE t1 t2 = filter (\t -> (bottom t1) == (top t)) (allVersions t2)



nextTile :: Edge -> (Tile, [Tile], [Tile]) -> (Tile -> [Tile] -> Bool) -> Maybe (Tile, [Tile], [Tile])
nextTile e (tile, tiles, inplay) checker = headMaybe $ mapMaybe (\(i, t) -> matchTile e tile (i, t) tiles) $ zip [0..] tiles
  where
    matchTile e t1 (i, t2) tiles = do
      let matches = findMatches e t1 t2
      let match' = headMaybe $ filter (\m -> checker m inplay) matches in case match' of
        Nothing -> Nothing
        Just match -> Just (match, removeAt i tiles, inplay ++ [match])

-- How do we implement the checker? Which edges should be checked for
-- a given index depends on the size of the board
-- So function needs the index of the current tile to check
-- as well as the board dim
-- We always enumerate board in row-major order.
-- Therefore we have to check left side and top side.
-- checkBoard :: Int -> (Int, Int) -> Tile -> [Tile] -> Bool
checkBoard (nx, ny) t [] = True
checkBoard (nx, ny) t ts = all checkNeighbor [0, 1]
  where
    i = length ts
    checkNeighbor 0 = if mod i nx == 0
                      then True
                      else if i-1 < 0
                           then True
                           else (left t) == (right (ts !! (i-1)))
                                
    checkNeighbor 1 = if i-nx < 0
                      then True
                      else (top t) == (bottom (ts !! (i-nx)))


getNeighbor :: Int -> (Int, Int) -> (Int, Int) -> Maybe Int
getNeighbor i dims (ix, iy) = do
  (ix', iy') <- unravel i dims
  ravel (ix' + ix, iy' + iy) dims

unravel :: Int -> (Int, Int) -> Maybe (Int, Int)
unravel i (nx, ny) = if i >= nx * ny || i < 0
                     then Nothing
                     else Just (mod i nx, div (i - (mod i nx)) ny)

ravel :: (Int, Int) -> (Int, Int) -> Maybe Int
ravel (ix, iy) (nx, ny) = if ix >= nx || ix < 0 || iy >= ny || iy < 0
                          then Nothing
                          else Just (iy * nx + ix)


calcEdge :: (Int, Int) -> Int -> Edge
calcEdge (nx, ny) index = if mod index nx == 0 then NE else RE -- if ix == nx - 1 then 

getConfig (nx, ny) index _ _ _ tilesInPlay | index == nx * ny = Just tilesInPlay
getConfig dims 0 _ curTile freeTiles tilesInPlay = Nothing
getConfig dims index edge curTile freeTiles tilesInPlay = getConfig dims newindex newedge newtile newfree newplay
  where
    maybeStuff = nextTile edge (curTile, freeTiles, tilesInPlay) checker
    checker = checkBoard dims
    (newtile, newfree, newplay) = case maybeStuff of
                                    Just (a, b, c) -> (a, b, c)
                                    Nothing -> (tilesInPlay !! (index - 1), freeTiles ++ [curTile], dropLast tilesInPlay)
    
    newedge = case maybeStuff of
                Just _ -> calcEdge dims (index + 1)
                Nothing -> calcEdge dims (index - 1)
    newindex = case maybeStuff of
                 Just _ -> index + 1
                 Nothing -> index - 1

runGetConfig dims [] _ = Nothing
runGetConfig dims tiles others = case whileNonEmpty testVersion (allVersions $ head tiles) of
  Just x -> Just x
  Nothing -> runGetConfig dims (tail tiles) (others ++ [head tiles])
  where
    whileNonEmpty f ls
      | length ls == 0 = Nothing
      | otherwise = case f (head ls) of
                          Just x -> Just x
                          Nothing -> whileNonEmpty f (tail ls)
    testVersion x = getConfig dims 1 RE x rest [x]
      where
        rest = (filter (\t -> (tileid t) /= (tileid x)) (tiles ++ others))


fst3 (a, b, c) = a
snd3 (a, b, c) = b
trd3 (a, b, c) = c

unjust Nothing = error "BLup"
unjust (Just ls) = ls

main = do
  tilestrs <- splitOn "\n\n" <$> readFile "input.txt"
  let tiles = map parseTile tilestrs
      checker = checkBoard (12, 12)
      results = unjust $ runGetConfig (12, 12) tiles []
  print $ product $ map tileid $ [results !! i | i <- [0, 11, 132, 143]]
  
  print "Peace on earth will come to stay, when we live Christmas every day."
