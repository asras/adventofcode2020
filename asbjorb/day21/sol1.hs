import System.IO
import Data.List.Split (splitOn)
import Data.Set (Set)
import qualified Data.Set as S
import Data.Map (Map)
import qualified Data.Map as M





type Food = ([String], [String])

parseFood :: String -> Food
parseFood f = (ings, allers)
  where
    [ings', allers'] = splitOn " (contains " f
    ings = words ings'
    allers = splitOn ", " $ filter (/=')') allers'


-- An allergen is a name and a set of possible ingredients it can be in
type Allergens = Map String (Set String)

-- Initially, all allergens could be in all ingredients.
-- Perhaps this is not the most efficient way to do it, memorywise.
initAllergens :: [Food] -> Allergens
initAllergens foods = M.fromList $ S.toList $ S.map (\al -> (al, allFoods)) als
  where
    als = S.fromList $ foldl (\acc f -> acc ++ (snd f)) [] foods
    allFoods = S.fromList $ foldl (\acc f -> acc ++ (fst f)) [] foods

-- Update allergen
-- An allergen is in /exactly one/ ingredient.
-- Therefore if it appears in a food we the allergen
-- has to be in that set of ingredients.
-- Applied over a multiple foods, we see that the final set
-- of possibilities for an allergen is the intersection of all
-- the ingredient sets for the foods in which it appears.

-- update takes on food and updates the set
update :: Allergens -> Food -> Allergens
update aller f = foldl (\m (al, pos) -> M.insert al (S.intersection (m M.! al) pos) m) aller (fromFood f)

fromFood :: Food -> [(String, Set String)]
fromFood f = [(al, S.fromList (fst f)) | al <- snd f]


smallin = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)"

main = do
  contents <- lines <$> readFile "input.txt"
  let foods = map parseFood contents
      allers = initAllergens foods
      finalallers = foldl update allers foods
      allergens = foldl (\set el -> S.union set (snd el)) S.empty $ M.toList finalallers
      ingredients = S.fromList $ foldl (\acc f -> acc ++ (fst f)) [] foods
      nonallergens = S.difference ingredients allergens
      count = foldl (\acc f -> acc + countIng (fst f) nonallergens) 0 foods
      countIng f set = foldl (\acc ing -> if S.member ing set then acc + 1 else acc) 0 f

  print count
  print "Christmas isn't a season. It's a feeling."
