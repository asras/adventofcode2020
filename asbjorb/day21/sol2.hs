import System.IO
import Data.List.Split (splitOn)
import Data.Set (Set)
import qualified Data.Set as S
import Data.Map (Map)
import qualified Data.Map as M
import Data.List (sort)


type Food = ([String], [String])

parseFood :: String -> Food
parseFood f = (ings, allers)
  where
    [ings', allers'] = splitOn " (contains " f
    ings = words ings'
    allers = splitOn ", " $ filter (/=')') allers'


-- An allergen is a name and a set of possible ingredients it can be in
type Allergens = Map String (Set String)

-- Initially, all allergens could be in all ingredients.
-- Perhaps this is not the most efficient way to do it, memorywise.
initAllergens :: [Food] -> Allergens
initAllergens foods = M.fromList $ S.toList $ S.map (\al -> (al, allFoods)) als
  where
    als = S.fromList $ foldl (\acc f -> acc ++ (snd f)) [] foods
    allFoods = S.fromList $ foldl (\acc f -> acc ++ (fst f)) [] foods

-- Update allergen
-- An allergen is in /exactly one/ ingredient.
-- Therefore if it appears in a food we the allergen
-- has to be in that set of ingredients.
-- Applied over a multiple foods, we see that the final set
-- of possibilities for an allergen is the intersection of all
-- the ingredient sets for the foods in which it appears.

-- update takes on food and updates the set
update :: Allergens -> Food -> Allergens
update aller f = foldl (\m (al, pos) -> M.insert al (S.intersection (m M.! al) pos) m) aller (fromFood f)

fromFood :: Food -> [(String, Set String)]
fromFood f = [(al, S.fromList (fst f)) | al <- snd f]

-- Function to take the final Allergens and reduce to
-- a list of allergen-ingredient pairs
reduceAllergens :: Allergens -> [(String, String)]
reduceAllergens allers = while anyRemaining scanForSingleton [] allerlist
  where
    allerlist = map (\(a,b) -> (a, S.toList b)) (M.toList allers)
    anyRemaining ls = length ls > 0
    scanForSingleton acc remaining = totup $ head $ filter ((==1) . length . snd) $ map setdiff remaining
      where
        setdiff (a, inls) = (a, S.toList $ S.difference (S.fromList inls) (S.fromList accls))
        accls = map snd acc
        totup (a, ls) = (a, head ls)
        
    while p f ls1 ls2
      | not (p ls2) = ls1
      | otherwise = while p f ((f ls1 ls2):ls1) (removeByFst (f ls1 ls2) ls2)

    removeByFst (x, _) ls = go x ls []
      where
        go x (y:ys) acc
          | x == (fst y) = acc ++ ys
          | otherwise = go x ys (acc ++ [y])



smallin = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)"

main = do
  contents <- lines <$> readFile "input.txt"
  let foods = map parseFood contents
      allers = initAllergens foods
      finalallers = foldl update allers foods

  putStrLn $ foldl (\s el -> s ++ (snd el) ++ ",") "" $ sort $ reduceAllergens finalallers
  print "Christmas isn't a season. It's a feeling."
