import System.IO
import Data.List.Split (splitOn)

type Deck = [Int]

parseDeck :: String -> Deck
parseDeck = (map read) . tail . lines


play :: Deck -> Deck -> Deck
play d1 [] = d1
play [] d2 = d2
play (c1:d1) (c2:d2)
  | c1 > c2 = play (d1 ++ [c1, c2]) d2
  | c2 > c1 = play d1 (d2 ++ [c2, c1])
  | otherwise = error $ "equals: " ++ (show c1) ++ " " ++ (show c2)




main = do
  [d1, d2] <- map parseDeck <$> splitOn "\n\n" <$> readFile "input.txt"
  let winner = play d1 d2
      score = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse winner)


  print score
  print "Christmas is doing a little something extra for someone."
