import System.IO
import Data.List.Split (splitOn)
import Data.Function

type Deck = [Int]

parseDeck :: String -> Deck
parseDeck = (map read) . tail . lines


play :: Deck -> Deck -> Deck
play d1 [] = d1
play [] d2 = d2
play (c1:d1) (c2:d2)
  | c1 > c2 = play (d1 ++ [c1, c2]) d2
  | c2 > c1 = play d1 (d2 ++ [c2, c1])
  | otherwise = error $ "equals: " ++ (show c1) ++ " " ++ (show c2)

play' recvar l1 l2
  | length l1 == 0 = l2
  | length l2 == 0 = l1
  | otherwise = if head l1 > head l2
                   then recvar (tail l1 ++ [head l1,head l2]) (tail l2)
                   else recvar (tail l1) (tail l2 ++ [head l2,head l1])


main = do
  [d1, d2] <- map parseDeck <$> splitOn "\n\n" <$> readFile "input.txt"
  let winner = play d1 d2
      score = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse winner)
      winner2 = fix play' d1 d2
      score2 = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse winner2)

  print score
  print score2
  print "Christmas is doing a little something extra for someone."
