{-# LANGUAGE BangPatterns #-}
import System.IO
import Data.List.Split (splitOn)
import Data.List

type Deck = [Int]


parseDeck :: String -> Deck
parseDeck = (map read) . tail . lines

-- d1 `isInfixOf` (drop (length d1) p1)
--  
--  | d1 `isInfixOf` (drop (length d1) p1) && d2 `isInfixOf` (drop (length d2) p2) = (1, d1)
play :: [(Deck, Deck)] -> Deck -> Deck -> (Int, Deck)
play _ d1 [] = (1, d1)
play _ [] d2 = (2, d2)
play ps d1 d2
  | elem (d1, d2) (tail ps) = (1, d1) -- We only check tail, because head is current configuration
play ps (c1:d1) (c2:d2)
  | c1 == c2 = error "equals"
  | length d1 < c1 || length d2 < c2 = if c1 > c2
                                       then play ((nd1, nd2):ps) nd1 nd2
                                       else play ((nd1', nd2'):ps) nd1' nd2'
  | otherwise = if (fst $ play [(take c1 d1, take c2 d2)] (take c1 d1) (take c2 d2)) == 1
                   then play ((nd1, nd2):ps) nd1 nd2
                   else play ((nd1', nd2'):ps) nd1' nd2'
    where
      -- If P1 wins:
      nd1 = (d1 ++ [c1, c2])
      nd2 = d2
      -- If P2 wins:
      nd1' = d1
      nd2' = (d2 ++ [c2, c1])
                                 

dk1 = [43, 19]
dk2 = [2, 29, 14]

main = do
  [d1, d2] <- map parseDeck <$> splitOn "\n\n" <$> readFile "input.txt"
  let winner = play [(d1, d2)] d1 d2
      score = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse (snd winner))

  print $ "Score is: " ++ (show score)
  print "Christmas is doing a little something extra for someone."
