{-# LANGUAGE BangPatterns #-}
import System.IO
import Data.List.Split (splitOn)
import Data.List
-- <<<<<<< HEAD
import Data.Vector (Vector)
import qualified Data.Vector as V

type Deck = Vector Int

parseDeck :: String -> Deck
parseDeck = V.fromList . (map (read :: String -> Int)) . tail . lines
-- =======

-- type Deck = [Int]


-- parseDeck :: String -> Deck
-- parseDeck = (map read) . tail . lines
-- >>>>>>> b8f072a7baae3ed1bba16a22f47a4f5f899331e7

-- d1 `isInfixOf` (drop (length d1) p1)
--  
--  | d1 `isInfixOf` (drop (length d1) p1) && d2 `isInfixOf` (drop (length d2) p2) = (1, d1)
-- <<<<<<< HEAD
play :: (Vector Deck, Vector Deck) -> Deck -> Deck -> (Int, Deck)
play _ d1 d2 | V.null d2 = (1, d1) | V.null d1 = (2, d2)
play (ps1, ps2) d1 d2
  | V.any (\(a, b) -> a == d1 && b == d2) (V.zip (V.tail ps1) (V.tail ps2)) = (1, d1)
play (ps1, ps2) d1 d2
  | c1 == c2 = error "equals"
  | (V.length d1) - 1 < c1 || (V.length d2) - 1 < c2 = if c1 > c2
                                       then play (V.cons nd1 ps1, V.cons nd2 ps2) nd1 nd2
                                       else play (V.cons nd1' ps1, V.cons nd2' ps2) nd1' nd2'
  | otherwise = if winner == 1
                   then play (V.cons nd1 ps1, V.cons nd2 ps2) nd1 nd2
                   else play (V.cons nd1' ps1, V.cons nd2' ps2) nd1' nd2'
    where
      c1 = V.head d1
      c2 = V.head d2
      (winner, _) = play (V.fromList [(V.tail d1)], V.fromList [(V.tail d2)]) (V.tail d1) (V.tail d2)
      nd1 = V.concat [d1, (V.fromList [c1, c2])]
      nd2 = d2
      nd1' = d1
      nd2' = V.concat [d2, (V.fromList [c2, c1])]
                                 
-- =======
-- play :: [(Deck, Deck)] -> Deck -> Deck -> (Int, Deck)
-- play _ d1 [] = (1, d1)
-- play _ [] d2 = (2, d2)
-- play ps (c1:d1) (c2:d2)
--   | elem ((c1:d1), (c2:d2)) ps = (1, (c1:d1))
--   | length d1 >= c1 && length d2 >= c2 = case winner of
--                                            1 -> play (((c1:d1), (c2:d2)):ps) (d1 ++ [c1, c2]) d2
--                                            2 -> play (((c1:d1), (c2:d2)):ps) d1 (d2 ++ [c2, c1])
--   | otherwise = if c1 > c2
--                 then play (((c1:d1), (c2:d2)):ps) (d1 ++ [c1, c2]) d2
--                 else play (((c1:d1), (c2:d2)):ps) d1 (d2 ++ [c2, c1])
--   where
--     (winner, _) = play [] (take c1 d1) (take c2 d2)

-- >>>>>>> b8f072a7baae3ed1bba16a22f47a4f5f899331e7

dk1 = [43, 19]
dk2 = [2, 29, 14]

-- <<<<<<< HEAD
-- ((map (read :: String -> Int)) . tail . lines)

main = do
  [d1, d2] <- map parseDeck <$> splitOn "\n\n" <$> readFile "smallinput.txt"
  let winner = play (V.fromList [d1], V.fromList [d2]) d1 d2
      score = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse (V.toList $ snd winner))

  print $ show d1
  print "========================="
  print $ show d2
  print score
-- =======
-- main = do
--   [d1, d2] <- map parseDeck <$> splitOn "\n\n" <$> readFile "input.txt"
--   let winner = play [] d1 d2
--       score = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse (snd winner))

--   print $ "Score is: " ++ (show score)
-- >>>>>>> b8f072a7baae3ed1bba16a22f47a4f5f899331e7
  print "Christmas is doing a little something extra for someone."
