{-# LANGUAGE BangPatterns #-}
import System.IO
import Data.List.Split (splitOn)
import Data.List

type Deck = [Int]


parseDeck :: String -> Deck
parseDeck = (map read) . tail . lines


play :: [(Deck, Deck)] -> Deck -> Deck -> (Int, Deck)
play _ d1 [] = (1, d1)
play _ [] d2 = (2, d2)
play ps d1 d2
  | any (\(a, b) -> a == d1 && b == d2) (tail ps) = (1, d1) -- We only check tail, because head is current configuration
play ps (c1:d1) (c2:d2)
  | c1 == c2 = error "equals"
  | otherwise = play ((nd1, nd2):ps) nd1 nd2
  where
    nd1 = if winner == 1 then d1 ++ [c1, c2] else d1
    nd2 = if winner == 2 then d2 ++ [c2, c1] else d2
    x = if winner /= 1 || winner /= 2 then error "blup" else Just "Doesn't matter"
    recurse = length d1 >= c1 && length d2 >= c2
    winner = if recurse then fst (play [(d1, d2)] d1 d2)
             else if c1 > c2 then 1 else 2
                                   

dk1 = [43, 19]
dk2 = [2, 29, 14]

main = do
  [d1, d2] <- map parseDeck <$> splitOn "\n\n" <$> readFile "smallinput.txt"
  let winner = play [(d1, d2)] d1 d2
      score = foldl (\acc (i, x) -> acc + i * x) 0 $ zip [1..] (reverse (snd winner))

  print $ "Score is: " ++ (show score)
  print "Christmas is doing a little something extra for someone."
