
mutable struct GameState
    cups :: Array{Int}
    active :: Int
end

function mymod(a, n)
    while a > n
        a = a - n
    end
    while a <= 0
        a = a + n
    end
    return a
end

function next_index(a, c1, c2, c3)
    k = 1
    N = 1_000_000
    while true
        ind = mod(a - k, N)
        if ind == 0
            ind = N
        end
        if ind in [c1, c2, c3]
            k += 1
            continue
        end
        return ind
    end
end

function crab!(gs :: GameState)
    active = gs.active
    cs = gs.cups

    c1 = cs[active] # Active points to c1
    c2 = cs[c1] # c1 points to c2
    c3 = cs[c2] # etc
    newactive = cs[c3] # By removing c1-3 the cup after c3 becomes the next active one
    cs[active] = newactive

    dest = next_index(active, c1, c2, c3)
    ndest = cs[dest]
    cs[dest] = c1
    cs[c3] = ndest
    gs.active = newactive
end



function main()
    # mylist = [8, 7, 1, 3, 6, 9, 4, 5, 2]
    
    mylinkedlist = [3, 10, 6, 5, 2, 9, 1, 7, 4]
    append!(mylinkedlist, range(11, length=1_000_000-10))
    append!(mylinkedlist, 8)

    state = GameState(mylinkedlist, 8)
    for i in 1:10_000_000
        crab!(state)
    end

    println("1 -> ", state.cups[1], " -> ", state.cups[state.cups[1]])
    println("Product: ", state.cups[1] * state.cups[state.cups[1]])
    return "blup"
end





