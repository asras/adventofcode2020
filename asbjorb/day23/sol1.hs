import System.IO
import Data.List

crab :: [Integer] -> [Integer]
crab values = take (length values) $ drop 1 $ cycle newvalues
  where
    current = head values
    take3 = take 3 $ drop 1 values
    remaining = head values : (drop 4 values)
    destination = while (\i -> not $ elem i remaining) stepDown (stepDown current)
      where
        stepDown i
          | i == 1 = 9
          | otherwise = i - 1
        while p f x
          | p x = while p f (f x)
          | otherwise = x
    Just index = elemIndex destination remaining
    newvalues = insertAt index remaining take3
    insertAt i ls vals
      | i == length ls - 1 = ls ++ vals
      | otherwise = let (l,r) = splitAt (i+1) ls in l ++ vals ++ r


game1 = [3,8,9,1,2,5,4,6,7]
game2 = [8,7,1,3,6,9,4,5,2]
  
main = do
  let crabbed = head $ drop 100 $ iterate crab game1

  print crabbed
  print "Christmas is the season for kindling the fire of hospitality."
