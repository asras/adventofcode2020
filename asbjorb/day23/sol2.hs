{-# LANGUAGE MultiWayIf #-}
import System.IO
import Data.List
import Data.Vector (Vector)
import qualified Data.Vector as V
import Control.Monad.ST
import qualified Data.Vector.Mutable as MV

data State = C { cups :: Vector Int, active :: Int} deriving Show

nextIndex i (c1, c2, c3) n = let x = head $ filter (\k -> k /= c1 && k/= c2 && k /= c3) $ map (\x -> mod (i - x) n) [1..] in if x < 0 || x >= n then error ("out of bounds: " ++ (show x)) else x

crab :: State -> State
crab (C cs a) = runST $ do
  mv <- V.unsafeThaw cs
  let get = MV.unsafeRead mv
      set = MV.unsafeWrite mv
  c1 <- get a -- a points to c1
  c2 <- get c1 -- c1 points to c2
  c3 <- get c2 -- ...
  a' <- get c3 -- ...
  set a a' -- make a point to a'. We are removing c1, c2, c3

  let dest = nextIndex a (c1, c2, c3) 1000000
  dest' <- get dest -- We want to insert c1, c2, c3 in front of dest, so get what dest is point to now
  set dest c1 -- Make dest point to c1
  set c3 dest' -- make c3 point to what dest pointed to
  
  cs' <- V.unsafeFreeze mv
  pure $ (C cs' a')


g1' = [3,8,9,1,2,5,4,6,7]
g1 = [2,7,8,0,1,4,3,5,6] -- We work with zero-indexing
cs1 = [1,4,7,5,3,6,2,8,0] -- Make linked list, 1 comes after 0 so cs1[0] = 1, etc

g2' = [8, 7, 1, 3, 6, 9, 4, 5, 2] ++ [10..1000000]
g2 = map (\x -> x-1) g2'


cs2 = (map (\x -> x-1) $ [3, 10, 6, 5, 2, 9, 1, 7, 4] ++ [11..1000000] ++ [8])

state1 = C (V.fromList cs1) 2
state2 = C (V.fromList cs2) 7

makeN :: State -> [Int]
makeN (C cs a) = map (+1) $ foldl (\acc i -> acc ++ [cs V.! (last acc)]) [0] [1..length cs - 1] 
  
main = do
  let crabbed@(C cs a) = head $ drop 10000000 $ iterate crab state2
  --print $ length cs2
  print $ [cs V.! 1, cs V.! (cs V.! 1)]
  print "Christmas is the season for kindling the fire of hospitality."

