{-# LANGUAGE MultiWayIf #-}
import System.IO
import Data.List
import Control.Monad (zipWithM_, foldM_, foldM)
import Control.Monad.ST
import Data.Array.ST --(MArray, STUArray)


crabStep :: (MArray a b m, Ix b, Num b) => a b b -> b -> m b 
crabStep marray active = do
  (low, high) <- getBounds marray
  c1 <- readArray marray active
  c2 <- readArray marray c1
  c3 <- readArray marray c2
  active' <- readArray marray c3
  writeArray marray active active'
  
  let dec x = if x == low then high else x - 1
      dest = until (\x -> not $ elem x [c1,c2,c3]) dec (dec active)
  dest' <- readArray marray dest

  writeArray marray dest c1
  writeArray marray c3 dest'

  return active'

makeLinkedList :: [Int] -> ST s (STUArray s Int Int)
makeLinkedList xs = do
  arr <- newArray_ (minimum xs, maximum xs)
  arr <$ zipWithM_ (writeArray arr) xs (drop 1 $ cycle xs)


g1 = [8, 7, 1, 3, 6, 9, 4, 5, 2] :: [Int]
g2 = g1 ++ [10..1000000] :: [Int]

part2 = runST $ do
  ll <- makeLinkedList g2
  foldM_ (const . crabStep ll) (head g1) $ replicate 10000000 ()
  (low, high) <- getBounds ll
  tolist <- mapM (readArray ll) [low..high]
  x <- readArray ll 1
  y <- readArray ll x
  pure $ (x, y)

convertToList :: (STUArray s Int Int) -> ST s ([Int])
convertToList ll = do
  (low, high) <- getBounds ll
  toList <- mapM (readArray ll) [low..high]
  pure $ toList

unlink :: [Int] -> [Int]
unlink ls = snd $ foldl (\(curi, acc) i -> (ls !! (curi-1), acc ++ [curi])) (head ls, []) ls 

main = do
  --print $ unlink $ runST ((makeLinkedList g1) >>= convertToList)
  print $ part2
  print "Christmas is the season for kindling the fire of hospitality."

