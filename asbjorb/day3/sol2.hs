import System.IO

-- Take a map represented as a string and check line i, char j
-- to see if it is a "tree" (#)
mapFunc :: String -> Int -> Int -> Bool
mapFunc content i j = access content i j == '#'
  where
    access content i j
      | i >= length (lines content) = error "row index too large"
    access content i j = lineI !! (mod j leng)
      where
        lineI = (lines content) !! i
        leng = length lineI

-- Small test case for mapfunc
testmap = unlines ["..#", "#.."]

testv1 = mapFunc testmap 0 0
testv2 = mapFunc testmap 0 2
testv3 = mapFunc testmap 1 0
testv4 = mapFunc testmap 1 1
testv5 = mapFunc testmap 0 3
testv6 = mapFunc testmap 0 3
-- /Small test


-- Next generate a list of all positions starting from 0,0
-- and moving with given slope.
-- Should stop generating when we hit the bottom.

type Position = (Int, Int)
type Slope = (Int, Int)

-- Take the number of positions to generate and the slope
-- Generate the positions
generatePositions :: Int -> Slope -> [Position]
generatePositions n (down, right) = [(down * i, right * i) | i <- [0..maxi]]
  where
    maxi = div n down - 1


-- Now we make a function to count the number of trees encountered at a given slope
-- Need the map as input
countTrees :: String -> Slope -> Int
countTrees map slope = (length . filter isTree) positions
  where
    isTree (i, j) = mapFunc map i j
    n = length $ lines map
    positions = generatePositions n slope





testslopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]


nonEmptyLines :: String -> [String]
nonEmptyLines contents = filter (\l -> length l > 0) $ lines contents
   
main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let numTrees = map (countTrees contents) testslopes
          totalProduct = product numTrees
      putStrLn ("The product: " ++ show totalProduct)


      putStrLn "        *        "
      putStrLn "        ^        "
      putStrLn "       /|\\      "
      putStrLn "      / | \\     "
      putStrLn "     /  |  \\    "
      putStrLn "    /   |   \\   "
      putStrLn "   /    |    \\  "
      putStrLn "  /     |     \\ "
      putStrLn " /      |      \\"
      putStrLn "Happy Holidays!")

