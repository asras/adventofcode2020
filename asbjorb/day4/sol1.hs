import System.IO
import Data.List.Split (splitOn)
import Data.List (isInfixOf)


passports :: String -> [String]
passports = splitOn "\n\n"

requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

passIsValid :: String -> Bool
passIsValid pass = all ((flip isInfixOf) pass) requiredFields 


main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let passes = passports contents
          numValid = length $ filter passIsValid passes  

      putStrLn ("Number of valid passes: " ++ show numValid)
      putStrLn "Fridays, am I right?")
