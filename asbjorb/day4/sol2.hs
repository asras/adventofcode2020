import System.IO
import Data.List.Split (splitOn)
import Data.List (isInfixOf)
import Text.Read (readMaybe)

type Passport = [(String, String)]

-- Helper function that splits on both opt1 and opt2
splitOnAnd :: String -> String -> String -> [String]
splitOnAnd opt1 opt2 input = splitted
  where
    -- input = "abc def\nghi"
    -- opt1 = " ", opt2 = "\n"
    -- splitFirst = ["abc", "def\nghi"]
    splitFirst = splitOn opt1 input
    -- map (splitOn opt2) splitFirst = [["abc"], ["def", "ghi"]]
    -- Use foldr (++) [] to flatten list -> ["abc", "def", "ghi"]
    splitted = foldr (++) [] $ map (splitOn opt2) splitFirst 
  

passports :: String -> [Passport]
passports contents = map convertKeyValues $ map (splitOnAnd " " "\n") $ splitOn "\n\n" contents
  where
    -- Convert non-empty lines
    convertKeyValues ls = map convertKeyValue $ filter (\s -> (length s) > 0) ls
    convertKeyValue s
      | length splitted /= 2 = ("Failed", s) -- Helpful hack for debugging
      | otherwise = (splitted !! 0, splitted !! 1) -- Unpack to Passport type
      where
        splitted = splitOn ":" s

requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]


-- Do key look-up in "assoc list"
assoc :: Eq a => a -> [(a, b)] -> Maybe (a, b)
assoc key [] = Nothing
assoc key (x:xs)
  | key == (fst x) = Just x
  | otherwise = assoc key xs

keyExists :: Eq a => a -> [(a, b)] -> Bool
keyExists key [] = False
keyExists key (x:xs)
  | key == (fst x) = True
  | otherwise = keyExists key xs


-- Make a validation function for each field
byrFormatValid :: String -> Bool
byrFormatValid s = length s == 4 && intvalCorrect
  where
    intvalCorrect = case readMaybe s :: Maybe Int of
                      Nothing -> False
                      Just i  -> i >= 1920 && i <= 2002

iyrFormatValid :: String -> Bool
iyrFormatValid s = length s == 4 && intvalCorrect
  where
    intvalCorrect = case readMaybe s :: Maybe Int of
                      Nothing -> False
                      Just i  -> i >= 2010 && i <= 2020

eyrFormatValid :: String -> Bool
eyrFormatValid s = length s == 4 && intvalCorrect
  where
    intvalCorrect = case readMaybe s :: Maybe Int of
                      Nothing -> False
                      Just i  -> i >= 2020 && i <= 2030


isNumber :: Char -> Bool
isNumber c = case readMaybe [c] :: Maybe Int of
               Nothing -> False
               Just _  -> True

hgtFormatValid :: String -> Bool
hgtFormatValid s = numberValid && unitValid
  where
    numbers = takeWhile isNumber s
    units   = dropWhile isNumber s

    number = read numbers :: Int
    numberValid = if units == "cm" then number >= 150 && number <= 193
                                   else number >= 59 && number <= 76
    unitValid = units == "cm" || units == "in"

    
hclFormatValid :: String -> Bool
hclFormatValid s = (head s == '#') && numbersValid
  where
    numbers = tail s
    correctLength = length numbers == 6
    isValidColorChar = \c -> elem c (['0'..'9'] ++ ['a'..'f'])
    charsValid = all isValidColorChar numbers
    numbersValid = correctLength && charsValid


eclFormatValid :: String -> Bool
eclFormatValid s = elem s ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

pidFormatValid :: String -> Bool
pidFormatValid s = all isNumber s && length s == 9
-- End of endless validation functions

-- Now we check
-- 1. Each required key is present: Use assoc to see that we dont get Nothing
-- 2. The value of is formatted correctly: Use validators defined above to check.
validChecks = [("byr", byrFormatValid), ("iyr", iyrFormatValid), ("eyr", eyrFormatValid),
               ("hgt", hgtFormatValid), ("hcl", hclFormatValid), ("ecl", eclFormatValid),
               ("pid", pidFormatValid)]
entryValid :: Passport -> (String, (String -> Bool)) -> Bool
entryValid pass (key, validator) = case assoc key pass of
                                    Nothing -> False -- Passport doesnt have required key
                                    Just (_, value) -> validator value -- Use validator to check format

fieldsAreValid :: Passport -> Bool
fieldsAreValid pass = all (entryValid pass) validChecks


main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let passes = passports contents
          valids = length $ filter fieldsAreValid passes
      putStrLn ("Number of valid passes: " ++ show valids)
      putStrLn "Fridays, am I right?")
