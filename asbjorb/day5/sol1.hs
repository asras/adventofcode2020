import System.IO

letter2Int :: Char -> Int
letter2Int 'F' = 0
letter2Int 'B' = 1
letter2Int 'R' = 1
letter2Int 'L' = 0
letter2Int s = error ("Dont use" ++ [s] ++ "here")

letters2Num :: String -> Int
letters2Num s = sum $ map (\(index, val) -> 2^(index) * val) $ zip [0..] (map letter2Int (reverse s))

string2RowCol :: String -> (Int, Int)
string2RowCol s = (letters2Num (take 7 s), letters2Num (drop 7 s))

seatId :: (Int, Int) -> Int
seatId (row, col) = row * 8 + col




main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let maxid = maximum $ map (seatId . string2RowCol) (lines contents)

      putStrLn ("The max id is: " ++ (show maxid))
      putStrLn "Santa Claus is Coming to Town!")

