import System.IO

letter2Int :: Char -> Int
letter2Int 'F' = 0
letter2Int 'B' = 1
letter2Int 'R' = 1
letter2Int 'L' = 0
letter2Int s = error ("Dont use" ++ [s] ++ "here")

letters2Num :: String -> Int
letters2Num s = sum $ map (\(index, val) -> 2^(index) * val) $ zip [0..] (map letter2Int (reverse s))

string2RowCol :: String -> (Int, Int)
string2RowCol s = (letters2Num (take 7 s), letters2Num (drop 7 s))

seatId :: (Int, Int) -> Int
seatId (row, col) = row * 8 + col


allRows = [0..127]
allCols = [0..7]
allSeats = [(r, c) | r <- allRows, c <- allCols]

mySeat :: [(Int, Int)] -> [(Int, Int)]
mySeat tickets = myseat
  where
    -- My seat is defined by not being in the list
    -- and seats with 'neighbor' ids being in the list
    neighbors id = [id - 1, id + 1]
    
    seatNotInList = \seat -> not (elem seat tickets)
    neighborsAreInList = \seat -> all (\i -> elem i (map seatId tickets)) (neighbors (seatId seat))
    myseat = filter seatNotInList $ filter neighborsAreInList allSeats

main = do
  withFile "input.txt" ReadMode (\handle -> do
      contents <- hGetContents handle
      let seats = map string2RowCol (lines contents)

      putStrLn $ show (map seatId $ mySeat seats)
      putStrLn ("My seat id is: " ++ (show "blup"))
      putStrLn "Santa Claus is Coming to Town!")

