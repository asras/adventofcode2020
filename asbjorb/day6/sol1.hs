import System.IO
import Data.List.Split (splitOn)
import qualified Data.Set as Set

groups :: String -> [String]
groups contents = splitOn "\n\n" contents

persons :: String -> [String]
persons group = lines group

uniqueAnswers :: String -> Set.Set Char
uniqueAnswers group = Set.fromList $ foldr (++) [] $ persons group

sumOfUniques :: [String] -> Int
sumOfUniques groups = sum $ map (Set.size . uniqueAnswers) groups



main = do
  contents <- readFile "input.txt"
  let part1 = sumOfUniques (groups contents)

  putStrLn $ "Answer to part 1 is: " ++ (show part1)

  putStrLn "    /\\__/\\  "
  putStrLn "     >^,^<    "
  putStrLn "      / \\    "
  putStrLn "     (___)___ "
  putStrLn "              "
  putStrLn "              "
  putStrLn "   |\\__/|    "
  putStrLn "  (  ^_^ )    "
  putStrLn "              "
  putStrLn "              "
  putStrLn "              "
  putStrLn "On the 6th Day of Christmas"
  putStrLn "Advent of Code gave to me"
  putStrLn "A puzzle involving weird list input"
