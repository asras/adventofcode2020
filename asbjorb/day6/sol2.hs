import System.IO
import Data.List.Split (splitOn)
import qualified Data.Set as Set

groups :: String -> [String]
groups contents = splitOn "\n\n" contents

persons :: String -> [String]
persons group = lines group

-- Return a string containing the answers that
-- everyone in the group answered 'yes' to
commonAnswers :: String -> String
commonAnswers group = combine fstPerson $ restPersons
  where
    (fstPerson:restPersons) = persons group
    combine ls [] = ls
    combine ls (sublist:rest) = combine (filter (\c -> elem c sublist) ls) rest

sumOfCommon :: [String] -> Int
sumOfCommon groups = sum $ map (length . commonAnswers) groups


main = do
  contents <- readFile "input.txt"
  let part2 = sumOfCommon (groups contents)

  putStrLn $ "Answer to part 2 is: " ++ (show part2)

  putStrLn "    /\\__/\\  "
  putStrLn "     >^,^<    "
  putStrLn "      / \\    "
  putStrLn "     (___)___ "
  putStrLn "              "
  putStrLn "              "
  putStrLn "   |\\__/|    "
  putStrLn "  (  ^_^ )    "
  putStrLn "              "
  putStrLn "              "
  putStrLn "              "
  putStrLn "On the 6th Day of Christmas"
  putStrLn "Advent of Code gave to me"
  putStrLn "A puzzle involving weird list input"
