import System.IO
import Data.Map (Map)
import qualified Data.Map as Map
import Data.List.Split (splitOn)
import qualified Data.Set as Set



data Bag = Bag {adj :: String, color :: String} deriving (Show, Eq, Ord)

-- We need to find the bags that can contain a shiny gold bag
-- The rules are "inverted" with respect to this requirements,
-- i.e. the rules tell you which bags a given bag can contain
-- not what they can be contained by.
-- Of course, we can just read the rules in an inverted way.
-- Let's make a list with Bag, [Bag] pairs where the [Bag]
-- tells which bags can contain Bag.
-- Actually let's use a dictionary (Map) for this.

type Rules = Map Bag [Bag]

addRule :: Rules -> Bag -> Bag -> Rules
addRule rules bag containingBag
  | Map.member bag rules = Map.adjust updater bag rules
  | otherwise = Map.insert bag [containingBag] rules
  where
    updater bags = containingBag:bags


-- A single rule is here given in the inverted form
-- i.e. it is a list of bags and the bag that can contain them
-- Right now the number of bags a bag can carry does not matter.
-- But it likely will in the next problem.
type Rule = ([Bag], Bag)


-- Get a rule from a line
parseRuleLine :: String -> Rule
parseRuleLine line = (bags, bag)
  where
    [firstPart, lastPart] = splitOn " contain " line

    [bagAdj, bagCol, _] = splitOn " " firstPart
    bag = Bag bagAdj bagCol

    -- Helper function to parser a substring
    -- of the bags list
    bagParser s = Bag adj col
      where
        splitted = splitOn " " s
        (adj, col) = if length splitted == 4
                     then (splitted !! 1, splitted !! 2)
                     else ("no", "other")
        --[_, adj, col, _] = splitOn " " s
        
    bags = map bagParser (splitOn ", " lastPart)


-- Helper function to take a rule and add to rules
insertRule :: Rules -> Rule -> Rules
insertRule rules (bags, bag) = newrules
  where
    newrules = foldr (\a currrules -> addRule currrules a bag) rules bags

-- Process each line by parsing and inserting
processLine :: String -> Rules -> Rules
processLine line rules = insertRule rules $ parseRuleLine line


-- Now a function that recursively gets a list of bags that
-- can contain a given bag
containerBags :: Rules -> Bag -> [Bag] -> [Bag]
containerBags rules bag accu
  | not (Map.member bag rules) = accu
  | otherwise = newbags
  where
    -- The bags I want are the ones I have already accumulated
    -- plus the ones in the bag's list, plus the recursive call
    -- to the ones in the bag's list
    
    bags = rules Map.! bag
    accu' = bags ++ accu
    newbags = foldr (\curr ac -> containerBags rules curr ac) accu' bags

main = do
  contents <- lines <$> readFile "input.txt"
  let rules' = Map.empty :: Rules
      rules = foldr processLine rules' contents
      target = Bag "shiny" "gold"
      containers = containerBags rules target []
      number = Set.size $ Set.fromList containers
  putStrLn ("Number of containers: " ++ (show number))
  putStrLn "I hate Mondays"
