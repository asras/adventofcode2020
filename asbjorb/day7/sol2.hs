import System.IO
import Data.Map (Map)
import qualified Data.Map as Map
import Data.List.Split (splitOn)
import qualified Data.Set as Set

-- TO SOLVE PART2:
-- We change "Rules" from mapping Bag -> Containers,
-- to mapping Bag -> Containees


data Bag = Bag {adj :: String, color :: String} deriving (Show, Eq, Ord)

-- We need to find the bags that can contain a shiny gold bag
-- The rules are "inverted" with respect to this requirements,
-- i.e. the rules tell you which bags a given bag can contain
-- not what they can be contained by.
-- Of course, we can just read the rules in an inverted way.
-- Let's make a list with Bag, [Bag] pairs where the [Bag]
-- tells which bags can contain Bag.
-- Actually let's use a dictionary (Map) for this.

type Rules = Map Bag [(Int, Bag)]

addRule :: Rules -> Bag -> [(Int, Bag)] -> Rules
addRule rules bag containees
  | Map.member bag rules = Map.adjust updater bag rules
  | otherwise = Map.insert bag containees rules
  where
    updater bags = containees ++ bags

-- Now the number of bags matters
-- A rule is now a bag, and a list of (Int, Bag) tuples
-- where the tuples show how many of the given bag
-- the bag can hold
type Rule = (Bag, [(Int, Bag)])


-- Get a rule from a line
parseRuleLine :: String -> Rule
parseRuleLine line = (bag, intbags)
  where
    [firstPart, lastPart] = splitOn " contain " line

    [bagAdj, bagCol, _] = splitOn " " firstPart
    bag = Bag bagAdj bagCol

    -- Helper function to parser a substring
    -- of the bags list
    bagParser s = (num, Bag adj col)
      where
        splitted = splitOn " " s
        (num, adj, col) = if length splitted == 4
                          then (read (splitted !! 0), splitted !! 1, splitted !! 2)
                          else (0, "no", "other")
        
    intbags = map bagParser (splitOn ", " lastPart)


-- Helper function to take a rule and add to rules
insertRule :: Rules -> Rule -> Rules
insertRule rules (bag, intbags) = addRule rules bag intbags

  
-- -- Process each line by parsing and inserting
processLine :: String -> Rules -> Rules
processLine line rules = insertRule rules $ parseRuleLine line

-- Now a function that recursively gets the total number
-- of containees
containeeBags :: Rules -> Bag -> Int
containeeBags rules bag
  | not (Map.member bag rules) = 0
  | otherwise = mynumber + theirnumbers
  where
    -- Get my number of containing bags
    -- And get the number that containees have
    -- Remember to multiply by number of bags that I have
    containees = rules Map.! bag
    mynumber = foldr (+) 0 $ map fst containees
    theirnumbers = sum $ map (\(num, bag) -> num * (containeeBags rules bag)) containees

main = do
  contents <- lines <$> readFile "input.txt"
  let rules' = Map.empty :: Rules
      rules = foldr processLine rules' contents
      target = Bag "shiny" "gold"
      number = containeeBags rules target
  putStrLn ("Number of containees: " ++ (show number))
  putStrLn "I hate Mondays"


{-
The solution for today was not so pretty.

I wonder if there is a nice data structure where we could handle
each part by simply switching "direction".


e.g. something like

data BagGraph = BagNode Bag [(Int, Bag)] [Bag]

The first list is containees and the second is containers.

Maybe it is not necessary to store both the forward and backward
directions but I don't know a good way to traverse a graph forwards
and backwards easily otherwise.

Maybe it would be easier to use a Map and just map to both
the forwards and the backwards directions.
-}
