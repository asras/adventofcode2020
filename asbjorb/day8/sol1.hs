import System.IO
import Data.List.Split (splitOn)

data ProgramState = P {currLine :: Int, acc :: Int} deriving Show
data Instruction = Nop | Acc Int | Jmp Int deriving Show


processLine :: Instruction -> ProgramState -> ProgramState
processLine Nop (P line acc)       = P (line + 1) acc
processLine (Acc inc) (P line acc) = P (line + 1) (acc + inc)
processLine (Jmp jmp) (P line acc) = P (line + jmp) acc

-- We want to keep track of visited lines
-- There must be some nice structure we can use for this
-- Not sure what it would be though.
-- Is my tracking-thing below just some kind of functor?

type TrackedState = (ProgramState, [Int])

-- After every step ls contained the lines that have already been processed
-- and state contains the next line to be processed
trackLine :: Instruction -> TrackedState -> TrackedState
trackLine ins (state, ls) = (processLine ins state, (currLine state):ls)

-- Maybe these functions are redundant...
-- Although you could say they represent the program (list of instructions)
-- and current 'memory' state
stepProgram :: ([Instruction], ProgramState) -> ([Instruction], ProgramState)
stepProgram (insts, state) = (insts, processLine inst state)
  where
    inst = insts !! (currLine state)

trackSteps :: ([Instruction], TrackedState) -> ([Instruction], TrackedState)
trackSteps (insts, state) = (insts, trackLine inst state)
  where
    state' = fst state
    inst = insts !! (currLine state')


stopAtFirstRepeatedLine :: [Instruction] -> TrackedState -> TrackedState
stopAtFirstRepeatedLine insts state = snd $ head $ filter seenBefore $ iterate trackSteps (insts, state)
  where
    seenBefore (_, (state', seen)) = (currLine state') `elem` (seen)
stopAtRep = stopAtFirstRepeatedLine -- Concise alias


(!$) x f = f x -- pipe operator
parseLine :: String -> Instruction
parseLine line = strip line !$ splitOn " " !$ parseParts
  where
    f = reverse . dropWhile (\c -> c == ' ')
    strip = f . f
    removePlus = filter (\c -> c /= '+') -- For some reason read doesnt like +
    parseParts ["nop", _] = Nop
    parseParts ["acc", x] = Acc (read (removePlus x) :: Int)
    parseParts ["jmp", x] = Jmp (read (removePlus x) :: Int)
    parseParts x = error ("Got weird input: " ++ (show x))



main = do
  contents <- lines <$> readFile "input.txt"
  let instructions = map parseLine contents
      startState = P 0 0
      finalState = fst $ stopAtRep instructions (startState, [])
  putStrLn ("The value of the accumulator is: " ++ (show (acc finalState)))
  putStrLn "Another Lockdown..."

