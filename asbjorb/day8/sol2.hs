import System.IO
import Data.List.Split (splitOn)

-- Simple solution: Generate all programs.
-- There will only be ~300 so it's doable.
-- Elegant solution: ????
-- I was thinking it might be possible to run
-- the program forwards and backwards simultaneously,
-- and check when they met.
-- That would work in the simple example but not in general.
-- Also, there doesnt seem to be a way to define running a jmp
-- backwards.
-- I think you would need a much more elaborate scaffolding
-- to do something like this, e.g. storing all possible
-- jmp destinations from replacing nop -> jmp.

-- To do the simple solution we need to store the argument
-- for nops.


data ProgramState = P {currLine :: Int, acc :: Int} deriving Show
data Instruction = Nop Int | Acc Int | Jmp Int deriving (Show, Eq)


processLine :: Instruction -> ProgramState -> ProgramState
processLine (Nop _) (P line acc)   = P (line + 1) acc
processLine (Acc inc) (P line acc) = P (line + 1) (acc + inc)
processLine (Jmp jmp) (P line acc) = P (line + jmp) acc

type TrackedState = (ProgramState, [Int])

-- After every step ls contained the lines that have already been processed
-- and state contains the next line to be processed
trackLine :: Instruction -> TrackedState -> TrackedState
trackLine ins (state, ls) = (processLine ins state, (currLine state):ls)

trackSteps :: ([Instruction], TrackedState) -> ([Instruction], TrackedState)
trackSteps (insts, state) = (insts, trackLine inst state)
  where
    state' = fst state
    inst = insts !! (currLine state')


stopAtFirstRepeatedLine :: [Instruction] -> TrackedState -> TrackedState
stopAtFirstRepeatedLine insts state = snd $ head $ filter seenBefore $ iterate trackSteps (insts, state)
  where
    seenBefore (_, (state', seen)) = (currLine state') `elem` (seen)
stopAtRep = stopAtFirstRepeatedLine -- Concise alias


(!$) x f = f x -- pipe operator
parseLine :: String -> Instruction
parseLine line = strip line !$ splitOn " " !$ parseParts
  where
    f = reverse . dropWhile (\c -> c == ' ')
    strip = f . f
    removePlus = filter (\c -> c /= '+') -- For some reason read doesnt like +
    parseParts ["nop", x] = Nop (read (removePlus x) :: Int)
    parseParts ["acc", x] = Acc (read (removePlus x) :: Int)
    parseParts ["jmp", x] = Jmp (read (removePlus x) :: Int)
    parseParts x = error ("Got weird input: " ++ (show x))


isNop (Nop _) = True
isNop _ = False
isJmp (Jmp _) = True
isJmp _ = False

-- Replace each nop with jmp
-- Replace each jmp with nop
allPrograms :: [Instruction] -> [[Instruction]]
allPrograms program = programs
  where
    nopLocs = filter (\(ind, x) -> isNop x) (zip [0..] program) !$ map fst
    jmpLocs = filter (\(ind, x) -> isJmp x) (zip [0..] program) !$ map fst

    replaceNop index ls = first ++ [newjmp] ++ rest
      where
        (first, (Nop x):rest) = splitAt index ls
        newjmp = Jmp x
    replaceJmp index ls = first ++ [newnop] ++ rest
      where
        (first, (Jmp x):rest) = splitAt index ls
        newnop = Nop x
        
    programs = [replaceNop i program | i <- nopLocs] ++ [replaceJmp i program | i <- jmpLocs]


type Program = ([Instruction], TrackedState)
data StepState = Done Program | Repeated Program | Running Program
stepProgram :: Program -> StepState
stepProgram (insts, (state, seen))
  | (currLine state) >= (length insts) = Done (insts, (state, seen))
  | (currLine state) `elem` seen = Repeated (insts, (state, seen))
  | otherwise = Running (insts, trackLine inst (state, seen))
  where
    inst = insts !! (currLine state)

stepUntilNotRunning (Done x) = Done x
stepUntilNotRunning (Repeated x) = Repeated x
stepUntilNotRunning (Running x) = stepUntilNotRunning (stepProgram x)
                   

-- A program terminates it ever reaches the last line
-- A program does not terminate if it reaches the same line twice
programTerminates :: [Instruction] -> (Bool, TrackedState)
programTerminates instructions = doesTerminate
  where
    start' = (P 0 0, [])
    start = Running (instructions, start')
    doesTerminate = case stepUntilNotRunning start of
                      Done (_, x) -> (True, x)
                      Repeated (_, x) -> (False, x)


main = do
  contents <- lines <$> readFile "input.txt"
  let instructions = map parseLine contents
      programs = allPrograms instructions
      finalState = map programTerminates programs !$ filter fst !$ map snd !$ head

  putStrLn ("Value of accumulator after termination: " ++ (show (acc $ fst finalState)))
  putStrLn "Another Lockdown..."

