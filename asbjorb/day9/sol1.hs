import System.IO


allPairs :: [Int] -> [[Int]]
allPairs [] = []
allPairs (x:xs) = [[x, other] | other <- xs] ++ allPairs xs

isSumOfPairs :: Int -> [Int] -> Bool
isSumOfPairs t ls = elem t $ map sum $ allPairs ls

getSublist :: Int -> [Int] -> [Int]
getSublist j ls = [ls !! i | i <- [j-25..j]]

xmasWeakness :: [Int] -> [Int]
xmasWeakness ls = map (\i -> ls !! i) $ filter checkSublist [25..length ls - 1]
  where
    pairSublist i = allPairs $ getSublist i ls
    checkSublist i = all (\sls -> (sum sls) /= (ls !! i)) (pairSublist i)



main = do
  contents <- lines <$> readFile "input.txt"
  let nums = map read contents :: [Int]

  print $ "The weakness is the number: " ++ show (head $ xmasWeakness nums)
  print "Christmas Bells are Ringing!"
