import System.IO


allPairs :: [Int] -> [[Int]]
allPairs [] = []
allPairs (x:xs) = [[x, other] | other <- xs] ++ allPairs xs

isSumOfPairs :: Int -> [Int] -> Bool
isSumOfPairs t ls = elem t $ map sum $ allPairs ls

getSublist :: Int -> [Int] -> [Int]
getSublist j ls = [ls !! i | i <- [j-25..j]]

xmasWeakness :: [Int] -> Int
xmasWeakness ls = head $ map (\i -> ls !! i) $ filter checkSublist [25..length ls - 1]
  where
    pairSublist i = allPairs $ getSublist i ls
    checkSublist i = all (\sls -> (sum sls) /= (ls !! i)) (pairSublist i)

getSublist' n j ls = [ls !! i | i <- [j-n..j]]

xmasWeakness' n ls = head $ map (\i -> ls !! i) $ filter checkSublist [n..length ls - 1]
  where
    pairSublist i = allPairs $ getSublist' n i ls
    checkSublist i = all (\sls -> (sum sls) /= (ls !! i)) (pairSublist i)


-- This was an extremely bad idea:
-- runningSums :: [Int] -> [(Int, [Int])]
-- runningSums [] = []
-- runningSums [x] = [(x, [x])]
-- runningSums (x:xs) = [(x, [x])] ++ [(x + osums, x:ols) | (osums, ols) <- otherRuns] ++ otherRuns
--   where
--     otherRuns = runningSums xs


hacking :: Int -> [Int] -> [Int] -> [[Int]]-> [[Int]]
hacking t [] (x:xs) acc
  | x < t = hacking t [x] xs acc
  | x == t = hacking t [] xs ([x]:acc)
  | otherwise = hacking t [] xs acc
hacking t currEls (x:xs) acc
  | (sum currEls) + x < t = hacking t (currEls++[x]) xs acc
  | (sum currEls) + x == t = hacking t (tail currEls) ((x:xs)) ((x:currEls):acc)
  | (sum currEls) + x > t = hacking t (tail currEls) ((x:xs)) acc
hacking t curr [] acc
  | (sum curr) == t = curr:acc
  | otherwise = acc


sumMinMax ls = (maximum ls) + (minimum ls)

main = do
  contents <- lines <$> readFile "input.txt"
  let nums = map read contents :: [Int]
      weakNumber = xmasWeakness nums
      
  print $ "Hackmaster24 says: " ++ show (map sumMinMax $ filter ((>2) . length) $ hacking weakNumber [] nums [])
  print "Christmas Bells are Ringing!"
