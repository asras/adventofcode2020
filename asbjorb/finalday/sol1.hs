



cryptStep :: Int -> Int -> Int
cryptStep subject val = mod (val * subject) 20201227





test = head $ drop 8 $ iterate (cryptStep 7) 1
test2 = head $ drop 11 $ iterate (cryptStep 7) 1


key1 = 2084668 :: Int
key2 = 3704642 :: Int


decrypt :: Int -> (Int, Int)
decrypt target = head $ filter (\(a, b) -> b == target) $ zip [0..] $ iterate (cryptStep 7) 1


main = do
  let lsize2 = fst $ decrypt key2
      
  print $ head $ drop lsize2 $ iterate (cryptStep key1) 1
  print "What a journey, merry christmas"
