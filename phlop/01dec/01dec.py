# import scipy.special
import itertools

# # Number of combinations
# binom = scipy.special.binom(len(expenses), 2)
# print(int(binom))

# Open puzzle input
with open('input.txt') as f: # open expense report as save lines
    expenses = f.readlines()

expenses_int = [int(i) for i in expenses]

# # Part 1
# for i, ii in itertools.combinations(expenses_int, 2):
#     # Sum of two elements that are equal to 2020 and multiplied by eachother
#     if i + ii == 2020:
#         print('Candidates = {}, {}'.format(i,ii))
#         print('1 dec. solution  = {}'.format(i*ii))

# Part 2
for i, ii, iii in itertools.combinations(expenses_int, 3):
    # Sum of two elements that are equal to 2020 and multiplied by eachother
    if i + ii + iii == 2020:
        print('Candidates = {}, {}, {}'.format(i,ii,iii))
        print('1 dec. solution  = {}'.format(i*ii*iii))
