import re

# Open advent code input
with open('input.txt', 'r') as f:
    fr = f.readlines()

# # Splup 01
# count_pass = 0 
# for line in fr:
#     # Split it oh yeah
#     line1, line2 = line.split(': ')
#     intv, ch = line1.split()
#     intv_min, intv_max = intv.split('-')

#     # occur = len(re.findall(ch, line2))
#     occur = line2.count(ch)

#     if int(intv_min) <= occur <= int(intv_max):
#         count_pass += 1

# print('Nr of correct passwords in input = {}'.format(count_pass))


# Splup 02
count_pass = 0 
for line in fr:
    # Split it oh yeah
    line1, line2 = line.split(': ')
    pos, ch = line1.split()
    pos1, pos2 = pos.split('-')

    if line2[int(pos1)-1]==ch and line2[int(pos2)-1]!=ch:
        count_pass += 1
    elif line2[int(pos1)-1]!=ch and line2[int(pos2)-1]==ch:
        count_pass += 1

print('Nr of correct passwords in input = {}'.format(count_pass))
