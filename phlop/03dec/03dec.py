# tree master philop

with open('input.txt', 'r') as f:
    fr = f.readlines()

### Task 1 Fint the trees
start = [0, 0] # start point
slope = [1, 3] # direction

# some initialization and lengths
col_nr = start[1] 
row_len = len(fr)
col_len = len(fr[0])-2 # minus newline and convert to zero index
count_trees = 0

for row_nr in range(start[0], row_len, slope[0]):

    row = list(fr[row_nr].strip('\n')) # convert to list

    # its a tree!
    if row[col_nr] == '#':
        count_trees +=1
        
    col_nr += slope[1]# update col nr

    # end of the grid, start from the beginning
    if col_nr > col_len:
        col_nr = col_nr % col_len-1 # remember zero index 

print('Number of trees encountered = {}'.format(count_trees))

#### Task 2
# Run the above code with slopes [1,1], [1,3], [1,7], [1,5], [2,1]
# and multiply the results
# Easy peasy


