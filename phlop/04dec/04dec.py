#### 04 dec oh yeah
import re

with open('input.txt', 'r') as f:
    fr = f.readlines()

# stuff
ind = 0
passports = {}
gotid = False

# matches_pass = ['byr', 'iyr', 'eyr', 'hgt',
#                 'hcl', 'ecl', 'pid', 'cid']
matches_northpole = ['byr', 'iyr', 'eyr', 'hgt',
                     'hcl', 'ecl', 'pid']

# create beatiful dict
for line in fr:
    if line == '\n':
        ind += 1
        gotid = False
        continue
    
    elif all(x in line for x in matches_northpole):
        id = 'id{}'.format(ind)
        passports[id] = {}

        pass_info = line.split()
        for info in pass_info:
            key, value = info.split(':')
            passports[id][key] = value
        ind += 1

    elif not gotid:
        id = 'id{}'.format(ind)
        passports[id] = {}
        gotid = True
        
        pass_info = line.split()
        for info in pass_info:
            key, value = info.split(':')
            passports[id][key] = value

    else:
        pass_info = line.split()
        for info in pass_info:
            key, value = info.split(':')
            passports[id][key] = value

#####
# Task 1
# Can be solved by coding 
# Solution is easy peasy
#####
# count valid entries from beatiful dict
count_valid = 0

for idnr in passports:
    
    if all(x in passports[idnr] for x in matches_northpole):
        count_valid += 1
#print(count_valid)

### Task 2
# count valid entries from beatiful dict

# Rules
    # byr (Birth Year) - four digits; at least 1920 and at most 2002.
    # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    # hgt (Height) - a number followed by either cm or in:
    #     If cm, the number must be at least 150 and at most 193.
    #     If in, the number must be at least 59 and at most 76.
    # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    # cid (Country ID) - ignored, missing or not.

count_valid = 0

for idnr in passports:
    valid = False
    if all(x in passports[idnr] for x in matches_northpole):

        if 1920 <= int(passports[idnr]['byr']) <= 2002 and len(passports[idnr]['byr']) == 4:
            byr_valid = True
            
        if 2010 <= int(passports[idnr]['iyr']) <= 2020 and len(passports[idnr]['iyr']) == 4:
            iyr_valid = True

        if 2020 <= int(passports[idnr]['eyr']) <= 2030 and len(passports[idnr]['eyr']) == 4:
            eyr_valid = True
            
        if 'cm' in passports[idnr]['hgt']:
            hgt = re.findall('\d+', passports[idnr]['hgt'])
            if 150 <= int(hgt[0]) <= 193:
                cm_valid = True

        if 'in' in passports[idnr]['hgt']:
            hgt = re.findall('\d+', passports[idnr]['hgt'])
            if 59 <= int(hgt[0]) <= 76:
                in_valid = True

        if passports[idnr]['ecl'] in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            ecl_valid = True

        if '#'......

                        

