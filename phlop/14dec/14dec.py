#####
# Task 1
# Can be solved by coding 
# Solution is easy peasy
#####

import numpy as np
import numpy.ma as ma
import re

with open('input.txt', 'r') as f:
    fr = f.readlines()

    
new_list = []
memory_list = []

for line in fr:

    line_str  = line.strip('\n').split(' = ')

    # get mask
    if 'mask' in line:       
        line_mask = line_str[1]
        mask_len = len(line_mask)
 
    else:
        memory = int(re.search(r'\d+', line_str[0]).group())
        line_bin = format(int(line_str[1]), 'b')
        # add appropiate leading zeros
        line_bin_lead = line_bin.zfill(mask_len)

        # standard run through bits and check up against mask
        ind = 0
        new_line = ''
        for x, y in zip(line_bin_lead, line_mask):
            if y == 'X':
                new_line += x
            else:
                new_line += y
            ind += 1

        # save in list, overwrite memory if it is the same
        if memory not in memory_list:
            new_list.append(int(new_line,2))
            memory_list.append(memory)
        else:
            ind_mem = memory_list.index(memory)
            new_list[ind_mem] = int(new_line,2)

# sum the unique values
sum_bum = 0
for i in new_list:
    sum_bum += i

print(sum_bum)
        
        






            

